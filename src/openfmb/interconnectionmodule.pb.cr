## Generated from interconnectionmodule/interconnectionmodule.proto for interconnectionmodule
require "protobuf"

require "./uml.pb.cr"
require "./wrappers.pb.cr"
require "./commonmodule.pb.cr"

module Interconnectionmodule
  
  struct InterconnectionPoint
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :black_start_enabled, Commonmodule::ControlSPC, 1
      optional :frequency_set_point_enabled, Commonmodule::ControlSPC, 2
      optional :island, Commonmodule::ControlSPC, 3
      optional :pct_hz_droop, Google::Protobuf::FloatValue, 4
      optional :pct_v_droop, Google::Protobuf::FloatValue, 5
      optional :ramp_rates, Commonmodule::RampRate, 6
      optional :reactive_pwr_set_point_enabled, Commonmodule::ControlSPC, 7
      optional :real_pwr_set_point_enabled, Commonmodule::ControlSPC, 8
      optional :voltage_set_point_enabled, Commonmodule::ControlSPC, 9
      optional :start_time, Commonmodule::Timestamp, 10
    end
  end
  
  struct InterconnectionCSG
    include ::Protobuf::Message
    
    contract_of "proto3" do
      repeated :crv_pts, InterconnectionPoint, 1
    end
  end
  
  struct InterconnectionControlScheduleFSCH
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :val_dcsg, InterconnectionCSG, 1
    end
  end
  
  struct InterconnectionScheduleFSCC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_fscc, Commonmodule::ControlFSCC, 1
      repeated :interconnection_control_schedule_fsch, InterconnectionControlScheduleFSCH, 2
    end
  end
  
  struct InterconnectionSchedule
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :identified_object, Commonmodule::IdentifiedObject, 1
      optional :check, Commonmodule::CheckConditions, 2
      optional :interconnection_schedule_fscc, InterconnectionScheduleFSCC, 3
    end
  end
  
  struct InterconnectionPlannedScheduleProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_message_info, Commonmodule::ControlMessageInfo, 1
      optional :requester_circuit_segment_service, Commonmodule::ApplicationSystem, 2
      optional :interconnection_schedule, InterconnectionSchedule, 3
      optional :tie_point, Commonmodule::ConductingEquipment, 4
      optional :responder_circuit_segment_service, Commonmodule::ApplicationSystem, 5
    end
  end
  
  struct InterconnectionRequestedScheduleProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_message_info, Commonmodule::ControlMessageInfo, 1
      optional :requester_circuit_segment_service, Commonmodule::ApplicationSystem, 2
      optional :interconnection_schedule, InterconnectionSchedule, 3
      optional :tie_point, Commonmodule::ConductingEquipment, 4
      optional :responder_circuit_segment_service, Commonmodule::ApplicationSystem, 5
    end
  end
  end
