## Generated from circuitsegmentservicemodule/circuitsegmentservicemodule.proto for circuitsegmentservicemodule
require "protobuf"

require "./uml.pb.cr"
require "./wrappers.pb.cr"
require "./commonmodule.pb.cr"

module Circuitsegmentservicemodule
  enum CircuitSegmentServiceModeKind
    CircuitSegmentServiceModeKindUNDEFINED = 0
    CircuitSegmentServiceModeKindNone = 1
    CircuitSegmentServiceModeKindAuto = 2
    CircuitSegmentServiceModeKindManual = 3
    CircuitSegmentServiceModeKindNetzero = 4
    CircuitSegmentServiceModeKindStart = 5
    CircuitSegmentServiceModeKindStop = 6
  end
  
  struct Optional_CircuitSegmentServiceModeKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :value, CircuitSegmentServiceModeKind, 1
    end
  end
  
  struct ENG_CircuitSegmentServiceModeKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :set_val, CircuitSegmentServiceModeKind, 1
      optional :set_val_extension, Google::Protobuf::StringValue, 2
    end
  end
  
  struct CircuitSegmentControlDCSC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node_for_control, Commonmodule::LogicalNodeForControl, 1
      optional :circuit_segment_service_mode, ENGCircuitSegmentServiceModeKind, 2
      optional :island, Commonmodule::ControlDPC, 3
    end
  end
  
  struct CircuitSegmentControl
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :identified_object, Commonmodule::IdentifiedObject, 1
      optional :check, Commonmodule::CheckConditions, 2
      optional :circuit_segment_control_dcsc, CircuitSegmentControlDCSC, 3
    end
  end
  
  struct CircuitSegmentControlProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_message_info, Commonmodule::ControlMessageInfo, 1
      optional :application_system, Commonmodule::ApplicationSystem, 2
      optional :circuit_segment_control, CircuitSegmentControl, 3
    end
  end
  
  struct CircuitSegmentEventDCSC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node, Commonmodule::LogicalNode, 1
      optional :circuit_segment_service_mode, ENGCircuitSegmentServiceModeKind, 2
      optional :island, Commonmodule::StatusSPS, 3
      optional :permissible_auto, Commonmodule::StatusSPS, 4
      optional :permissible_manual, Commonmodule::StatusSPS, 5
      optional :permissible_netzero, Commonmodule::StatusSPS, 6
      optional :permissible_start, Commonmodule::StatusSPS, 7
      optional :permissible_stop, Commonmodule::StatusSPS, 8
    end
  end
  
  struct CircuitSegmentEvent
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :identified_object, Commonmodule::IdentifiedObject, 1
      optional :circuit_segment_event_dcsc, CircuitSegmentEventDCSC, 2
    end
  end
  
  struct CircuitSegmentEventProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :event_message_info, Commonmodule::EventMessageInfo, 1
      optional :application_system, Commonmodule::ApplicationSystem, 2
      optional :circuit_segment_event, CircuitSegmentEvent, 3
    end
  end
  
  struct CircuitSegmentStatusDCSC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node, Commonmodule::LogicalNode, 1
      optional :circuit_segment_service_mode, ENGCircuitSegmentServiceModeKind, 2
      optional :island, Commonmodule::StatusDPS, 3
      optional :permissible_auto, Commonmodule::StatusSPS, 4
      optional :permissible_manual, Commonmodule::StatusSPS, 5
      optional :permissible_netzero, Commonmodule::StatusSPS, 6
      optional :permissible_start, Commonmodule::StatusSPS, 7
      optional :permissible_stop, Commonmodule::StatusSPS, 8
    end
  end
  
  struct CircuitSegmentStatus
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :identified_object, Commonmodule::IdentifiedObject, 1
      optional :circuit_segment_status_dcsc, CircuitSegmentStatusDCSC, 2
    end
  end
  
  struct CircuitSegmentStatusProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :status_message_info, Commonmodule::StatusMessageInfo, 1
      optional :application_system, Commonmodule::ApplicationSystem, 2
      optional :circuit_segment_status, CircuitSegmentStatus, 3
    end
  end
  end
