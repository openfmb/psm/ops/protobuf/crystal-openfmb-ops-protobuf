# Crystal language protobuf for OpenFMB operational use cases

This repository contains the [Crystal](https://crystal-lang.org/) programming language Protocol Buffer (protobuf) definitions based on the OpenFMB operational use case data model located [here](https://gitlab.com/openfmb/data-models/ops).

## Including in your project

Add the following to your project's `shard.yaml`:

```
dependencies:
  protobuf:
    github: jeromegn/protobuf.cr
    version: ~> 2.1.2
  openfmb:
    gitlab: openfmb/psm/ops/protobuf/crystal-openfmb-ops-protobuf
    version: ~> 1.0.0
```

## Using

After adding the depencency in your project, you are ready to include the protobuf definitions into your source files like this:

```crystal
require "protobuf"
require "openfmb"
```

More to come...

## Copyright

See the COPYRIGHT file for copyright information of information contained in this repository.

## License

Unless otherwise noted, all files in this repository are distributed under the Apache Version 2.0 license found in the LICENSE file.
