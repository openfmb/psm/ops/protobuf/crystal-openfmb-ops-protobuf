## Generated from solarmodule/solarmodule.proto for solarmodule
require "protobuf"

require "./uml.pb.cr"
require "./wrappers.pb.cr"
require "./commonmodule.pb.cr"

module Solarmodule
  
  struct SolarInverter
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :conducting_equipment, Commonmodule::ConductingEquipment, 1
    end
  end
  
  struct SolarCapabilityConfiguration
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :source_capability_configuration, Commonmodule::SourceCapabilityConfiguration, 1
    end
  end
  
  struct SolarCapabilityOverride
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :identified_object, Commonmodule::IdentifiedObject, 1
      optional :solar_capability_configuration, SolarCapabilityConfiguration, 2
    end
  end
  
  struct SolarCapabilityOverrideProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :capability_message_info, Commonmodule::CapabilityMessageInfo, 1
      optional :solar_capability_override, SolarCapabilityOverride, 2
      optional :solar_inverter, SolarInverter, 3
    end
  end
  
  struct SolarCapabilityRatings
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :source_capability_ratings, Commonmodule::SourceCapabilityRatings, 1
    end
  end
  
  struct SolarCapability
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :nameplate_value, Commonmodule::NameplateValue, 1
      optional :solar_capability_configuration, SolarCapabilityConfiguration, 2
      optional :solar_capability_ratings, SolarCapabilityRatings, 3
    end
  end
  
  struct SolarCapabilityProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :capability_message_info, Commonmodule::CapabilityMessageInfo, 1
      optional :solar_capability, SolarCapability, 2
      optional :solar_inverter, SolarInverter, 3
    end
  end
  
  struct SolarPoint
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :mode, Commonmodule::ENGGridConnectModeKind, 2
      optional :ramp_rates, Commonmodule::RampRate, 5
      optional :reset, Commonmodule::ControlSPC, 8
      optional :state, Commonmodule::OptionalStateKind, 9
      optional :enter_service_operation, Commonmodule::EnterServiceAPC, 12
      optional :hz_w_operation, Commonmodule::HzWAPC, 13
      optional :limit_w_operation, Commonmodule::LimitWAPC, 14
      optional :p_f_operation, Commonmodule::PFSPC, 15
      optional :tm_hz_trip_operation, Commonmodule::TmHzCSG, 16
      optional :tm_volt_trip_operation, Commonmodule::TmVoltCSG, 17
      optional :v_ar_operation, Commonmodule::VarSPC, 18
      optional :volt_var_operation, Commonmodule::VoltVarCSG, 19
      optional :volt_w_operation, Commonmodule::VoltWCSG, 20
      optional :w_var_operation, Commonmodule::WVarCSG, 21
      optional :black_start_enabled, Commonmodule::ControlSPC, 22
      optional :w_operation, Commonmodule::WSPC, 24
    end
  end
  
  struct SolarCurvePoint
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control, SolarPoint, 1
      optional :start_time, Commonmodule::ControlTimestamp, 2
    end
  end
  
  struct SolarCSG
    include ::Protobuf::Message
    
    contract_of "proto3" do
      repeated :crv_pts, SolarCurvePoint, 1
    end
  end
  
  struct SolarControlScheduleFSCH
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :val_dcsg, SolarCSG, 1
    end
  end
  
  struct SolarControlFSCC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_fscc, Commonmodule::ControlFSCC, 1
      optional :solar_control_schedule_fsch, SolarControlScheduleFSCH, 2
    end
  end
  
  struct SolarControl
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_value, Commonmodule::ControlValue, 1
      optional :check, Commonmodule::CheckConditions, 2
      optional :solar_control_fscc, SolarControlFSCC, 3
    end
  end
  
  struct SolarControlProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_message_info, Commonmodule::ControlMessageInfo, 1
      optional :solar_control, SolarControl, 2
      optional :solar_inverter, SolarInverter, 3
    end
  end
  
  struct SolarDiscreteControlPV
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node_for_control, Commonmodule::LogicalNodeForControl, 1
      optional :control, SolarPoint, 2
    end
  end
  
  struct SolarDiscreteControl
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_value, Commonmodule::ControlValue, 1
      optional :check, Commonmodule::CheckConditions, 2
      optional :solar_discrete_control_pv, SolarDiscreteControlPV, 3
    end
  end
  
  struct SolarDiscreteControlProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_message_info, Commonmodule::ControlMessageInfo, 1
      optional :solar_discrete_control, SolarDiscreteControl, 2
      optional :solar_inverter, SolarInverter, 3
    end
  end
  
  struct SolarPointStatus
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :frequency_set_point_enabled, Commonmodule::StatusSPS, 1
      optional :mode, Commonmodule::ENGGridConnectModeKind, 2
      optional :pct_hz_droop, Google::Protobuf::FloatValue, 3
      optional :pct_v_droop, Google::Protobuf::FloatValue, 4
      optional :ramp_rates, Commonmodule::RampRate, 5
      optional :reactive_pwr_set_point_enabled, Commonmodule::StatusSPS, 6
      optional :real_pwr_set_point_enabled, Commonmodule::StatusSPS, 7
      optional :state, Commonmodule::OptionalStateKind, 8
      optional :voltage_set_point_enabled, Commonmodule::StatusSPS, 9
      optional :black_start_enabled, Commonmodule::ControlSPC, 10
      optional :enter_service_operation, Commonmodule::EnterServiceAPC, 11
      optional :hz_w_operation, Commonmodule::HzWPoint, 12
      optional :limit_w_operation, Commonmodule::LimitWAPC, 13
      optional :p_f_operation, Commonmodule::PFSPC, 14
      optional :sync_back_to_grid, Commonmodule::ControlSPC, 15
      optional :tm_hz_trip_operation, Commonmodule::TmHzCSG, 16
      optional :tm_volt_trip_operation, Commonmodule::TmVoltCSG, 17
      optional :v_ar_operation, Commonmodule::VarSPC, 18
      optional :volt_var_operation, Commonmodule::VoltVarCSG, 19
      optional :volt_w_operation, Commonmodule::VoltWCSG, 20
      optional :w_var_operation, Commonmodule::WVarCSG, 21
    end
  end
  
  struct SolarEventAndStatusZGEN
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node_for_event_and_status, Commonmodule::LogicalNodeForEventAndStatus, 1
      optional :aux_pwr_st, Commonmodule::StatusSPS, 2
      optional :dynamic_test, Commonmodule::ENSDynamicTestKind, 3
      optional :emg_stop, Commonmodule::StatusSPS, 4
      optional :point_status, SolarPointStatus, 5
      optional :alrm, Commonmodule::OptionalAlrmKind, 6
      optional :gn_syn_st, Commonmodule::StatusSPS, 7
      optional :grid_connection_state, Commonmodule::OptionalGridConnectionStateKind, 8
      optional :man_alrm_info, Google::Protobuf::StringValue, 9
      optional :operating_state, Commonmodule::OptionalOperatingStateKind, 10
    end
  end
  
  struct SolarEventZGEN
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :solar_event_and_status_zgen, SolarEventAndStatusZGEN, 1
      optional :gri_mod, Commonmodule::ENGGridConnectModeKind, 2
    end
  end
  
  struct SolarEvent
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :event_value, Commonmodule::EventValue, 1
      optional :solar_event_zgen, SolarEventZGEN, 2
    end
  end
  
  struct SolarEventProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :event_message_info, Commonmodule::EventMessageInfo, 1
      optional :solar_event, SolarEvent, 2
      optional :solar_inverter, SolarInverter, 3
    end
  end
  
  struct SolarReading
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :conducting_equipment_terminal_reading, Commonmodule::ConductingEquipmentTerminalReading, 1
      optional :phase_mmtn, Commonmodule::PhaseMMTN, 2
      optional :reading_mmtr, Commonmodule::ReadingMMTR, 3
      optional :reading_mmxu, Commonmodule::ReadingMMXU, 4
    end
  end
  
  struct SolarReadingProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :reading_message_info, Commonmodule::ReadingMessageInfo, 1
      optional :solar_inverter, SolarInverter, 2
      optional :solar_reading, SolarReading, 3
    end
  end
  
  struct SolarStatusZGEN
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :solar_event_and_status_zgen, SolarEventAndStatusZGEN, 1
      optional :gri_mod, Commonmodule::ENGGridConnectModeKind, 2
    end
  end
  
  struct SolarStatus
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :status_value, Commonmodule::StatusValue, 1
      optional :solar_status_zgen, SolarStatusZGEN, 2
    end
  end
  
  struct SolarStatusProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :status_message_info, Commonmodule::StatusMessageInfo, 1
      optional :solar_inverter, SolarInverter, 2
      optional :solar_status, SolarStatus, 3
    end
  end
  end
