## Generated from metermodule/metermodule.proto for metermodule
require "protobuf"

require "./uml.pb.cr"
require "./commonmodule.pb.cr"

module Metermodule
  
  struct MeterReading
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :conducting_equipment_terminal_reading, Commonmodule::ConductingEquipmentTerminalReading, 1
      optional :phase_mmtn, Commonmodule::PhaseMMTN, 2
      optional :reading_mmtr, Commonmodule::ReadingMMTR, 3
      optional :reading_mmxu, Commonmodule::ReadingMMXU, 4
    end
  end
  
  struct MeterReadingProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :reading_message_info, Commonmodule::ReadingMessageInfo, 1
      optional :meter, Commonmodule::Meter, 2
      optional :meter_reading, MeterReading, 3
    end
  end
  end
