## Generated from switchmodule/switchmodule.proto for switchmodule
require "protobuf"

require "./uml.pb.cr"
require "./commonmodule.pb.cr"

module Switchmodule
  
  struct SwitchDiscreteControlXSWI
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node_for_control, Commonmodule::LogicalNodeForControl, 1
      optional :pos, Commonmodule::PhaseDPC, 2
      optional :reset_protection_pickup, Commonmodule::ControlSPC, 3
    end
  end
  
  struct SwitchDiscreteControl
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_value, Commonmodule::ControlValue, 1
      optional :check, Commonmodule::CheckConditions, 2
      optional :switch_discrete_control_xswi, SwitchDiscreteControlXSWI, 3
    end
  end
  
  struct ProtectedSwitch
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :conducting_equipment, Commonmodule::ConductingEquipment, 1
    end
  end
  
  struct SwitchDiscreteControlProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_message_info, Commonmodule::ControlMessageInfo, 1
      optional :protected_switch, ProtectedSwitch, 2
      optional :switch_discrete_control, SwitchDiscreteControl, 3
    end
  end
  
  struct SwitchEventXSWI
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node_for_event_and_status, Commonmodule::LogicalNodeForEventAndStatus, 1
      optional :dynamic_test, Commonmodule::ENSDynamicTestKind, 2
      optional :pos, Commonmodule::PhaseDPS, 3
    end
  end
  
  struct SwitchEvent
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :event_value, Commonmodule::EventValue, 1
      optional :switch_event_xswi, SwitchEventXSWI, 2
    end
  end
  
  struct SwitchEventProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :event_message_info, Commonmodule::EventMessageInfo, 1
      optional :protected_switch, ProtectedSwitch, 2
      optional :switch_event, SwitchEvent, 3
    end
  end
  
  struct SwitchReading
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :conducting_equipment_terminal_reading, Commonmodule::ConductingEquipmentTerminalReading, 1
      optional :diff_reading_mmxu, Commonmodule::ReadingMMXU, 2
      optional :phase_mmtn, Commonmodule::PhaseMMTN, 3
      optional :reading_mmtr, Commonmodule::ReadingMMTR, 4
      optional :reading_mmxu, Commonmodule::ReadingMMXU, 5
    end
  end
  
  struct SwitchReadingProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :reading_message_info, Commonmodule::ReadingMessageInfo, 1
      optional :protected_switch, ProtectedSwitch, 2
      repeated :switch_reading, SwitchReading, 3
    end
  end
  
  struct SwitchStatusXSWI
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node_for_event_and_status, Commonmodule::LogicalNodeForEventAndStatus, 1
      optional :dynamic_test, Commonmodule::ENSDynamicTestKind, 2
      optional :pos, Commonmodule::PhaseDPS, 4
      optional :protection_pickup, Commonmodule::PhaseSPS, 5
    end
  end
  
  struct SwitchStatus
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :status_value, Commonmodule::StatusValue, 1
      optional :switch_status_xswi, SwitchStatusXSWI, 2
    end
  end
  
  struct SwitchStatusProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :status_message_info, Commonmodule::StatusMessageInfo, 1
      optional :protected_switch, ProtectedSwitch, 2
      optional :switch_status, SwitchStatus, 3
    end
  end
  end
