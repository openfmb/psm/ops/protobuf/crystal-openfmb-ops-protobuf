## Generated from capbankmodule/capbankmodule.proto for capbankmodule
require "protobuf"

require "./uml.pb.cr"
require "./commonmodule.pb.cr"

module Capbankmodule
  
  struct CapBankSystem
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :conducting_equipment, Commonmodule::ConductingEquipment, 1
    end
  end
  
  struct CapBankControlYPSH
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :amp_lmt, Commonmodule::PhaseSPC, 1
      optional :amp_thd_hi, Commonmodule::PhaseAPC, 2
      optional :amp_thd_lo, Commonmodule::PhaseAPC, 3
      optional :ctl_mode_auto, Commonmodule::ControlSPC, 4
      optional :ctl_mode_ovr_rd, Commonmodule::ControlSPC, 5
      optional :ctl_mode_rem, Commonmodule::ControlSPC, 6
      optional :dir_mode, Commonmodule::OptionalDirectionModeKind, 7
      optional :pos, Commonmodule::PhaseSPC, 8
      optional :temp_lmt, Commonmodule::ControlSPC, 9
      optional :temp_thd_hi, Commonmodule::ControlAPC, 10
      optional :temp_thd_lo, Commonmodule::ControlAPC, 11
      optional :v_ar_lmt, Commonmodule::PhaseSPC, 12
      optional :v_ar_thd_hi, Commonmodule::PhaseAPC, 13
      optional :v_ar_thd_lo, Commonmodule::PhaseAPC, 14
      optional :vol_lmt, Commonmodule::PhaseSPC, 15
      optional :vol_thd_hi, Commonmodule::PhaseAPC, 16
      optional :vol_thd_lo, Commonmodule::PhaseAPC, 17
    end
  end
  
  struct CapBankPoint
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control, CapBankControlYPSH, 1
      optional :start_time, Commonmodule::Timestamp, 2
    end
  end
  
  struct CapBankCSG
    include ::Protobuf::Message
    
    contract_of "proto3" do
      repeated :crv_pts, CapBankPoint, 1
    end
  end
  
  struct CapBankControlScheduleFSCH
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :val_csg, CapBankCSG, 1
    end
  end
  
  struct CapBankControlFSCC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_fscc, Commonmodule::ControlFSCC, 1
      optional :cap_bank_control_schedule_fsch, CapBankControlScheduleFSCH, 2
    end
  end
  
  struct CapBankControl
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_value, Commonmodule::ControlValue, 1
      optional :check, Commonmodule::CheckConditions, 2
      optional :cap_bank_control_fscc, CapBankControlFSCC, 3
    end
  end
  
  struct CapBankControlProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_message_info, Commonmodule::ControlMessageInfo, 1
      optional :cap_bank_control, CapBankControl, 2
      optional :cap_bank_system, CapBankSystem, 3
    end
  end
  
  struct CapBankDiscreteControlYPSH
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node_for_control, Commonmodule::LogicalNodeForControl, 1
      optional :control, CapBankControlYPSH, 2
    end
  end
  
  struct CapBankDiscreteControl
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_value, Commonmodule::ControlValue, 1
      optional :check, Commonmodule::CheckConditions, 2
      optional :cap_bank_discrete_control_ypsh, CapBankDiscreteControlYPSH, 3
    end
  end
  
  struct CapBankDiscreteControlProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_message_info, Commonmodule::ControlMessageInfo, 1
      optional :cap_bank_control, CapBankDiscreteControl, 2
      optional :cap_bank_system, CapBankSystem, 3
    end
  end
  
  struct CapBankEventAndStatusYPSH
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node_for_event_and_status, Commonmodule::LogicalNodeForEventAndStatus, 1
      optional :amp_lmt, Commonmodule::PhaseSPS, 2
      optional :ctl_mode, Commonmodule::OptionalControlModeKind, 3
      optional :dir_rev, Commonmodule::PhaseSPS, 4
      optional :dynamic_test, Commonmodule::ENSDynamicTestKind, 5
      optional :pos, Commonmodule::PhaseDPS, 6
      optional :temp_lmt, Commonmodule::PhaseSPS, 7
      optional :v_ar_lmt, Commonmodule::PhaseSPS, 8
      optional :vol_lmt, Commonmodule::PhaseSPS, 9
    end
  end
  
  struct CapBankEvent
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :event_value, Commonmodule::EventValue, 1
      optional :cap_bank_event_and_status_ypsh, CapBankEventAndStatusYPSH, 2
    end
  end
  
  struct CapBankEventProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :event_message_info, Commonmodule::EventMessageInfo, 1
      optional :cap_bank_event, CapBankEvent, 2
      optional :cap_bank_system, CapBankSystem, 3
    end
  end
  
  struct CapBankReading
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :conducting_equipment_terminal_reading, Commonmodule::ConductingEquipmentTerminalReading, 1
      optional :phase_mmtn, Commonmodule::PhaseMMTN, 2
      optional :reading_mmtr, Commonmodule::ReadingMMTR, 3
      optional :reading_mmxu, Commonmodule::ReadingMMXU, 4
      optional :secondary_reading_mmxu, Commonmodule::ReadingMMXU, 5
    end
  end
  
  struct CapBankReadingProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :reading_message_info, Commonmodule::ReadingMessageInfo, 1
      optional :cap_bank_reading, CapBankReading, 2
      optional :cap_bank_system, CapBankSystem, 3
    end
  end
  
  struct CapBankStatus
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :status_value, Commonmodule::StatusValue, 1
      optional :cap_bank_event_and_status_ypsh, CapBankEventAndStatusYPSH, 2
    end
  end
  
  struct CapBankStatusProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :status_message_info, Commonmodule::StatusMessageInfo, 1
      optional :cap_bank_status, CapBankStatus, 2
      optional :cap_bank_system, CapBankSystem, 3
    end
  end
  end
