## Generated from commonmodule/commonmodule.proto for commonmodule
require "protobuf"

require "./uml.pb.cr"
require "./wrappers.pb.cr"

module Commonmodule
  enum FaultDirectionKind
    FaultDirectionKindUNDEFINED = 0
    FaultDirectionKindUnknown = 1
    FaultDirectionKindForward = 2
    FaultDirectionKindBackward = 3
    FaultDirectionKindBoth = 4
  end
  enum PhaseFaultDirectionKind
    PhaseFaultDirectionKindUNDEFINED = 0
    PhaseFaultDirectionKindUnknown = 1
    PhaseFaultDirectionKindForward = 2
    PhaseFaultDirectionKindBackward = 3
  end
  enum UnitSymbolKind
    UnitSymbolKindNone = 0
    UnitSymbolKindMeter = 2
    UnitSymbolKindGram = 3
    UnitSymbolKindAmp = 5
    UnitSymbolKindDeg = 9
    UnitSymbolKindRad = 10
    UnitSymbolKindDegC = 23
    UnitSymbolKindFarad = 25
    UnitSymbolKindSec = 27
    UnitSymbolKindHenry = 28
    UnitSymbolKindV = 29
    UnitSymbolKindOhm = 30
    UnitSymbolKindJoule = 31
    UnitSymbolKindNewton = 32
    UnitSymbolKindHz = 33
    UnitSymbolKindW = 38
    UnitSymbolKindPa = 39
    UnitSymbolKindM2 = 41
    UnitSymbolKindSiemens = 53
    UnitSymbolKindVA = 61
    UnitSymbolKindVAr = 63
    UnitSymbolKindWPerVA = 65
    UnitSymbolKindVAh = 71
    UnitSymbolKindWh = 72
    UnitSymbolKindVArh = 73
    UnitSymbolKindHzPerS = 75
    UnitSymbolKindWPerS = 81
    UnitSymbolKindOther = 100
    UnitSymbolKindAh = 106
    UnitSymbolKindMin = 159
    UnitSymbolKindHour = 160
    UnitSymbolKindM3 = 166
    UnitSymbolKindWPerM2 = 179
    UnitSymbolKindDegF = 279
    UnitSymbolKindMph = 500
  end
  enum UnitMultiplierKind
    UnitMultiplierKindUNDEFINED = 0
    UnitMultiplierKindNone = 1
    UnitMultiplierKindOther = 2
    UnitMultiplierKindCenti = 3
    UnitMultiplierKindDeci = 4
    UnitMultiplierKindGiga = 5
    UnitMultiplierKindKilo = 6
    UnitMultiplierKindMega = 7
    UnitMultiplierKindMicro = 8
    UnitMultiplierKindMilli = 9
    UnitMultiplierKindNano = 10
    UnitMultiplierKindPico = 11
    UnitMultiplierKindTera = 12
  end
  enum PhaseCodeKind
    PhaseCodeKindNone = 0
    PhaseCodeKindOther = 1
    PhaseCodeKindN = 16
    PhaseCodeKindC = 32
    PhaseCodeKindCN = 33
    PhaseCodeKindAC = 40
    PhaseCodeKindACN = 41
    PhaseCodeKindB = 64
    PhaseCodeKindBN = 65
    PhaseCodeKindBC = 66
    PhaseCodeKindBCN = 97
    PhaseCodeKindA = 128
    PhaseCodeKindAN = 129
    PhaseCodeKindAB = 132
    PhaseCodeKindABN = 193
    PhaseCodeKindABC = 224
    PhaseCodeKindABCN = 225
    PhaseCodeKindS2 = 256
    PhaseCodeKindS2N = 257
    PhaseCodeKindS1 = 512
    PhaseCodeKindS1N = 513
    PhaseCodeKindS12 = 768
    PhaseCodeKindS12N = 769
  end
  enum ValidityKind
    ValidityKindUNDEFINED = 0
    ValidityKindGood = 1
    ValidityKindInvalid = 2
    ValidityKindReserved = 3
    ValidityKindQuestionable = 4
  end
  enum SourceKind
    SourceKindUNDEFINED = 0
    SourceKindProcess = 1
    SourceKindSubstituted = 2
  end
  enum TimeAccuracyKind
    TimeAccuracyKindUNDEFINED = 0
    TimeAccuracyKindT0 = 7
    TimeAccuracyKindT1 = 10
    TimeAccuracyKindT2 = 14
    TimeAccuracyKindT3 = 16
    TimeAccuracyKindT4 = 18
    TimeAccuracyKindT5 = 20
    TimeAccuracyKindUnspecified = 31
  end
  enum ScheduleParameterKind
    ScheduleParameterKindUNDEFINED = 0
    ScheduleParameterKindNone = 1
    ScheduleParameterKindOther = 2
    ScheduleParameterKindANetMag = 3
    ScheduleParameterKindANeutMag = 4
    ScheduleParameterKindAPhsAMag = 5
    ScheduleParameterKindAPhsBMag = 6
    ScheduleParameterKindAPhsCMag = 7
    ScheduleParameterKindHzMag = 8
    ScheduleParameterKindPFNetMag = 9
    ScheduleParameterKindPFNeutMag = 10
    ScheduleParameterKindPFPhsAMag = 11
    ScheduleParameterKindPFPhsBMag = 12
    ScheduleParameterKindPFPhsCMag = 13
    ScheduleParameterKindPhVNetAng = 14
    ScheduleParameterKindPhVNetMag = 15
    ScheduleParameterKindPhVNeutAng = 16
    ScheduleParameterKindPhVNeutMag = 17
    ScheduleParameterKindPhVPhsAAng = 18
    ScheduleParameterKindPhVPhsAMag = 19
    ScheduleParameterKindPhVPhsBAng = 20
    ScheduleParameterKindPhVPhsBMag = 21
    ScheduleParameterKindPhVPhsCAng = 22
    ScheduleParameterKindPhVPhsCMag = 23
    ScheduleParameterKindPPVPhsABAng = 24
    ScheduleParameterKindPPVPhsABMag = 25
    ScheduleParameterKindPPVPhsBCAng = 26
    ScheduleParameterKindPPVPhsBCMag = 27
    ScheduleParameterKindPPVPhsCAAng = 28
    ScheduleParameterKindPPVPhsCAMag = 29
    ScheduleParameterKindVANetMag = 30
    ScheduleParameterKindVANeutMag = 31
    ScheduleParameterKindVAPhsAMag = 32
    ScheduleParameterKindVAPhsBMag = 33
    ScheduleParameterKindVAPhsCMag = 34
    ScheduleParameterKindVArNetMag = 35
    ScheduleParameterKindVArNeutMag = 36
    ScheduleParameterKindVArPhsAMag = 37
    ScheduleParameterKindVArPhsBMag = 38
    ScheduleParameterKindVArPhsCMag = 39
    ScheduleParameterKindWNetMag = 40
    ScheduleParameterKindWNeutMag = 41
    ScheduleParameterKindWPhsAMag = 42
    ScheduleParameterKindWPhsBMag = 43
    ScheduleParameterKindWPhsCMag = 44
  end
  enum CalcMethodKind
    CalcMethodKindUNDEFINED = 0
    CalcMethodKindPCLASS = 11
    CalcMethodKindMCLASS = 12
    CalcMethodKindDIFF = 13
  end
  enum GridConnectModeKind
    GridConnectModeKindUNDEFINED = 0
    GridConnectModeKindCSI = 1
    GridConnectModeKindVCVSI = 2
    GridConnectModeKindCCVSI = 3
    GridConnectModeKindNone = 98
    GridConnectModeKindOther = 99
    GridConnectModeKindVSIPQ = 2000
    GridConnectModeKindVSIVF = 2001
    GridConnectModeKindVSIISO = 2002
  end
  enum PFSignKind
    PFSignKindUNDEFINED = 0
    PFSignKindIEC = 1
    PFSignKindEEI = 2
  end
  enum BehaviourModeKind
    BehaviourModeKindUNDEFINED = 0
    BehaviourModeKindOn = 1
    BehaviourModeKindBlocked = 2
    BehaviourModeKindTest = 3
    BehaviourModeKindTestBlocked = 4
    BehaviourModeKindOff = 5
  end
  enum DERGeneratorStateKind
    DERGeneratorStateKindUNDEFINED = 0
    DERGeneratorStateKindNotOperating = 1
    DERGeneratorStateKindOperating = 2
    DERGeneratorStateKindStartingUp = 3
    DERGeneratorStateKindShuttingDown = 4
    DERGeneratorStateKindAtDisconnectLevel = 5
    DERGeneratorStateKindRampingInPower = 6
    DERGeneratorStateKindRampingInReactivePower = 7
    DERGeneratorStateKindStandby = 8
    DERGeneratorStateKindNotApplicableUnknown = 98
    DERGeneratorStateKindOther = 99
  end
  enum DynamicTestKind
    DynamicTestKindUNDEFINED = 0
    DynamicTestKindNone = 1
    DynamicTestKindTesting = 2
    DynamicTestKindOperating = 3
    DynamicTestKindFailed = 4
  end
  enum HealthKind
    HealthKindUNDEFINED = 0
    HealthKindNone = 1
    HealthKindOK = 2
    HealthKindWarning = 3
    HealthKindAlarm = 4
  end
  enum SwitchingCapabilityKind
    SwitchingCapabilityKindUNDEFINED = 0
    SwitchingCapabilityKindNone = 1
    SwitchingCapabilityKindOpen = 2
    SwitchingCapabilityKindClose = 3
    SwitchingCapabilityKindOpenAndClose = 4
  end
  enum DbPosKind
    DbPosKindUNDEFINED = 0
    DbPosKindTransient = 1
    DbPosKindClosed = 2
    DbPosKindOpen = 3
    DbPosKindInvalid = 4
  end
  enum RecloseActionKind
    RecloseActionKindUNDEFINED = 0
    RecloseActionKindIdle = 1
    RecloseActionKindCycling = 2
    RecloseActionKindLockout = 3
  end
  enum NorOpCatKind
    NorOpCatKindUNDEFINED = 0
    NorOpCatKindA = 1
    NorOpCatKindB = 2
  end
  enum AbnOpCatKind
    AbnOpCatKindUNDEFINED = 0
    AbnOpCatKindI = 1
    AbnOpCatKindII = 2
    AbnOpCatKindIII = 3
  end
  enum AlrmKind
    AlrmKindGroundFault = 0
    AlrmKindDcOverVoltage = 1
    AlrmKindAcDisconnectOpen = 2
    AlrmKindDcDisconnectOpen = 3
    AlrmKindGridDisconnect = 4
    AlrmKindCabinetOpen = 5
    AlrmKindManualShutdown = 6
    AlrmKindOverTemperature = 7
    AlrmKindFrequencyAboveLimit = 8
    AlrmKindFrequencyUnderLimit = 9
    AlrmKindAcVoltageAboveLimit = 10
    AlrmKindAcVoltageUnderLimit = 11
    AlrmKindBlownStringFuseOnInput = 12
    AlrmKindUnderTemperature = 13
    AlrmKindGenericMemoryOrCommunicationError = 14
    AlrmKindHardwareTestFailure = 15
    AlrmKindManufacturerAlarm = 16
  end
  enum ControlModeKind
    ControlModeKindUNDEFINED = 0
    ControlModeKindAuto = 1
    ControlModeKindManual = 2
    ControlModeKindOverride = 3
    ControlModeKindRemote = 4
  end
  enum DirectionModeKind
    DirectionModeKindUNDEFINED = 0
    DirectionModeKindLockedForward = 1
    DirectionModeKindLockedReverse = 2
    DirectionModeKindReverseIdle = 3
    DirectionModeKindBidirectional = 4
    DirectionModeKindNeutralIdle = 5
    DirectionModeKindCogeneration = 6
    DirectionModeKindReactiveBidirectional = 7
    DirectionModeKindBiasBidirectional = 8
    DirectionModeKindBiasCogeneration = 9
    DirectionModeKindReverseCogeneration = 10
  end
  enum GridConnectionStateKind
    GridConnectionStateKindDisconnected = 0
    GridConnectionStateKindConnected = 1
  end
  enum OperatingStateKind
    OperatingStateKindUNDEFINED = 0
    OperatingStateKindOff = 1
    OperatingStateKindDisconnectedAndStandby = 2
    OperatingStateKindDisconnectedAndAvailable = 3
    OperatingStateKindDisconnectedAndAuthorized = 4
    OperatingStateKindStartingAndSynchronizing = 5
    OperatingStateKindConnectedAndIdle = 6
    OperatingStateKindConnectedAndGenerating = 7
    OperatingStateKindConnectedAndConsuming = 8
    OperatingStateKindStopping = 9
    OperatingStateKindDisconnectedAndBlocked = 10
    OperatingStateKindDisconnectedAndInMaintenance = 11
    OperatingStateKindCeasedToEnergize = 12
    OperatingStateKindFailed = 13
  end
  enum ReactivePowerControlKind
    ReactivePowerControlKindUNDEFINED = 0
    ReactivePowerControlKindAdvanced = 1
    ReactivePowerControlKindDroop = 2
    ReactivePowerControlKindVoltage = 3
    ReactivePowerControlKindReactivePower = 4
    ReactivePowerControlKindPowerFactor = 5
  end
  enum RealPowerControlKind
    RealPowerControlKindUNDEFINED = 0
    RealPowerControlKindAdvanced = 1
    RealPowerControlKindDroop = 2
    RealPowerControlKindIsochronous = 3
    RealPowerControlKindRealPower = 4
  end
  enum StateKind
    StateKindUNDEFINED = 0
    StateKindOff = 1
    StateKindOn = 2
    StateKindStandby = 3
  end
  enum VoltLimitModeKind
    VoltLimitModeKindUNDEFINED = 0
    VoltLimitModeKindOff = 1
    VoltLimitModeKindHighLimitOnly = 2
    VoltLimitModeKindLowLimitOnly = 3
    VoltLimitModeKindHighLowLimits = 4
    VoltLimitModeKindIvvcHighLimitOnly = 5
    VoltLimitModeKindIvvcLowLimitOnly = 6
    VoltLimitModeKindIvvcHighLowLimits = 7
  end
  
  struct Optional_FaultDirectionKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :value, FaultDirectionKind, 1
    end
  end
  
  struct Optional_PhaseFaultDirectionKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :value, PhaseFaultDirectionKind, 1
    end
  end
  
  struct ACD
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :dir_general, FaultDirectionKind, 1
      optional :dir_neut, OptionalPhaseFaultDirectionKind, 2
      optional :dir_phs_a, OptionalPhaseFaultDirectionKind, 3
      optional :dir_phs_b, OptionalPhaseFaultDirectionKind, 4
      optional :dir_phs_c, OptionalPhaseFaultDirectionKind, 5
      optional :general, :bool, 6
      optional :neut, Google::Protobuf::BoolValue, 7
      optional :phs_a, Google::Protobuf::BoolValue, 8
      optional :phs_b, Google::Protobuf::BoolValue, 9
      optional :phs_c, Google::Protobuf::BoolValue, 10
    end
  end
  
  struct IdentifiedObject
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :description, Google::Protobuf::StringValue, 1
      optional :m_rid, Google::Protobuf::StringValue, 2
      optional :name, Google::Protobuf::StringValue, 3
    end
  end
  
  struct ACDCTerminal
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :identified_object, IdentifiedObject, 1
      optional :connected, Google::Protobuf::BoolValue, 2
      optional :sequence_number, Google::Protobuf::Int32Value, 3
    end
  end
  
  struct Optional_UnitSymbolKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :value, UnitSymbolKind, 1
    end
  end
  
  struct Optional_UnitMultiplierKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :value, UnitMultiplierKind, 1
    end
  end
  
  struct ActivePower
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :multiplier, OptionalUnitMultiplierKind, 1
      optional :unit, OptionalUnitSymbolKind, 2
      optional :value, Google::Protobuf::FloatValue, 3
    end
  end
  
  struct Optional_PhaseCodeKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :value, PhaseCodeKind, 1
    end
  end
  
  struct Unit
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :multiplier, OptionalUnitMultiplierKind, 1
      optional :si_unit, UnitSymbolKind, 2
    end
  end
  
  struct Optional_ValidityKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :value, ValidityKind, 1
    end
  end
  
  struct DetailQual
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :bad_reference, :bool, 1
      optional :failure, :bool, 2
      optional :inaccurate, :bool, 3
      optional :inconsistent, :bool, 4
      optional :old_data, :bool, 5
      optional :oscillatory, :bool, 6
      optional :out_of_range, :bool, 7
      optional :overflow, :bool, 8
    end
  end
  
  struct Optional_SourceKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :value, SourceKind, 1
    end
  end
  
  struct Quality
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :detail_qual, DetailQual, 1
      optional :operator_blocked, :bool, 2
      optional :source, SourceKind, 3
      optional :test, :bool, 4
      optional :validity, ValidityKind, 5
    end
  end
  
  struct Optional_TimeAccuracyKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :value, TimeAccuracyKind, 1
    end
  end
  
  struct TimeQuality
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :clock_failure, :bool, 1
      optional :clock_not_synchronized, :bool, 2
      optional :leap_seconds_known, :bool, 3
      optional :time_accuracy, TimeAccuracyKind, 4
    end
  end
  
  struct Timestamp
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :seconds, :uint64, 2
      optional :tq, TimeQuality, 3
      optional :nanoseconds, :uint32, 4
    end
  end
  
  struct MV
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :mag, :double, 1
      optional :q, Quality, 2
      optional :t, Timestamp, 3
      optional :units, Unit, 4
    end
  end
  
  struct LogicalNode
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :identified_object, IdentifiedObject, 1
    end
  end
  
  struct AnalogEventAndStatusGGIO
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node, LogicalNode, 1
      optional :an_in, MV, 2
      optional :phase, OptionalPhaseCodeKind, 3
    end
  end
  
  struct NamedObject
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :description, Google::Protobuf::StringValue, 1
      optional :name, Google::Protobuf::StringValue, 2
    end
  end
  
  struct ApplicationSystem
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :named_object, NamedObject, 1
      optional :m_rid, :string, 2
    end
  end
  
  struct ASG
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :set_mag, :double, 1
    end
  end
  
  struct BCR
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :act_val, :int64, 1
      optional :q, Quality, 2
      optional :t, Timestamp, 3
    end
  end
  
  struct StatusSPS
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :q, Quality, 1
      optional :st_val, :bool, 2
      optional :t, Timestamp, 3
    end
  end
  
  struct BooleanEventAndStatusGGIO
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node, LogicalNode, 1
      optional :ind, StatusSPS, 2
      optional :phase, OptionalPhaseCodeKind, 3
    end
  end
  
  struct MessageInfo
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :identified_object, IdentifiedObject, 1
      optional :message_time_stamp, Timestamp, 2
    end
  end
  
  struct CapabilityMessageInfo
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :message_info, MessageInfo, 1
    end
  end
  
  struct CheckConditions
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :interlock_check, Google::Protobuf::BoolValue, 1
      optional :synchro_check, Google::Protobuf::BoolValue, 2
    end
  end
  
  struct ClearingTime
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :seconds, :uint64, 2
      optional :nanoseconds, :uint32, 3
    end
  end
  
  struct Vector
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :ang, Google::Protobuf::DoubleValue, 1
      optional :mag, :double, 2
    end
  end
  
  struct CMV
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :c_val, Vector, 1
      optional :q, Quality, 2
      optional :t, Timestamp, 3
    end
  end
  
  struct ConductingEquipment
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :named_object, NamedObject, 1
      optional :m_rid, :string, 2
    end
  end
  
  struct Terminal
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :a_cdc_terminal, ACDCTerminal, 1
      optional :phases, OptionalPhaseCodeKind, 2
    end
  end
  
  struct ConductingEquipmentTerminalReading
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :terminal, Terminal, 1
    end
  end
  
  struct ControlAPC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :ctl_val, :double, 1
    end
  end
  
  struct ControlDPC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :ctl_val, :bool, 1
    end
  end
  
  struct ControlTimestamp
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :seconds, :uint64, 2
      optional :nanoseconds, :uint32, 3
    end
  end
  
  struct Optional_ScheduleParameterKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :value, ScheduleParameterKind, 1
    end
  end
  
  struct ENG_ScheduleParameter
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :schedule_parameter_type, ScheduleParameterKind, 1
      optional :value, :double, 2
    end
  end
  
  struct SchedulePoint
    include ::Protobuf::Message
    
    contract_of "proto3" do
      repeated :schedule_parameter, ENGScheduleParameter, 1
      optional :start_time, ControlTimestamp, 2
    end
  end
  
  struct ScheduleCSG
    include ::Protobuf::Message
    
    contract_of "proto3" do
      repeated :sch_pts, SchedulePoint, 1
    end
  end
  
  struct ControlScheduleFSCH
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :val_acsg, ScheduleCSG, 1
    end
  end
  
  struct LogicalNodeForControl
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node, LogicalNode, 1
    end
  end
  
  struct ControlFSCC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node_for_control, LogicalNodeForControl, 1
      optional :control_schedule_fsch, ControlScheduleFSCH, 2
      optional :island_control_schedule_fsch, ControlScheduleFSCH, 3
    end
  end
  
  struct ControlINC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :ctl_val, :int32, 1
    end
  end
  
  struct ControlING
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :set_val, :int32, 1
      optional :units, Unit, 2
    end
  end
  
  struct ControlISC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :ctl_val, :int32, 1
    end
  end
  
  struct ControlMessageInfo
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :message_info, MessageInfo, 1
    end
  end
  
  struct ControlSPC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :ctl_val, :bool, 1
    end
  end
  
  struct ControlValue
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :identified_object, IdentifiedObject, 1
      optional :mod_blk, Google::Protobuf::BoolValue, 3
      optional :reset, Google::Protobuf::BoolValue, 4
    end
  end
  
  struct CumulativeTime
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :seconds, :uint64, 2
      optional :nanoseconds, :uint32, 3
    end
  end
  
  struct DateTimeInterval
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :end, Google::Protobuf::Int64Value, 1
      optional :start, Google::Protobuf::Int64Value, 2
    end
  end
  
  struct DEL
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :phs_ab, CMV, 1
      optional :phs_bc, CMV, 2
      optional :phs_ca, CMV, 3
    end
  end
  
  struct PhaseDPC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :phs3, ControlDPC, 1
      optional :phs_a, ControlDPC, 2
      optional :phs_b, ControlDPC, 3
      optional :phs_c, ControlDPC, 4
    end
  end
  
  struct DiscreteControlXCBR
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node_for_control, LogicalNodeForControl, 1
      optional :pos, PhaseDPC, 2
      optional :protection_mode, ControlINC, 3
      optional :reclose_enabled, ControlSPC, 4
      optional :reset_protection_pickup, ControlSPC, 5
    end
  end
  
  struct EnergyConsumer
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :conducting_equipment, ConductingEquipment, 1
      optional :operating_limit, Google::Protobuf::StringValue, 2
    end
  end
  
  struct Optional_CalcMethodKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :value, CalcMethodKind, 1
    end
  end
  
  struct ENG_CalcMethodKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :set_val, CalcMethodKind, 1
    end
  end
  
  struct Optional_GridConnectModeKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :value, GridConnectModeKind, 1
    end
  end
  
  struct ENG_GridConnectModeKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :set_val, GridConnectModeKind, 1
      optional :set_val_extension, Google::Protobuf::StringValue, 2
    end
  end
  
  struct Optional_PFSignKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :value, PFSignKind, 1
    end
  end
  
  struct ENG_PFSignKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :set_val, PFSignKind, 1
    end
  end
  
  struct Optional_BehaviourModeKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :value, BehaviourModeKind, 1
    end
  end
  
  struct ENS_BehaviourModeKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :q, Quality, 1
      optional :st_val, BehaviourModeKind, 2
      optional :t, Timestamp, 3
    end
  end
  
  struct Optional_DERGeneratorStateKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :value, DERGeneratorStateKind, 1
    end
  end
  
  struct ENS_DERGeneratorStateKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :q, Quality, 1
      optional :st_val, DERGeneratorStateKind, 2
      optional :t, Timestamp, 3
    end
  end
  
  struct Optional_DynamicTestKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :value, DynamicTestKind, 1
    end
  end
  
  struct ENS_DynamicTestKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :q, Quality, 1
      optional :st_val, DynamicTestKind, 2
      optional :t, Timestamp, 3
    end
  end
  
  struct ENS_GridConnectModeKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :st_val, GridConnectModeKind, 1
      optional :st_val_extension, :string, 2
    end
  end
  
  struct Optional_HealthKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :value, HealthKind, 1
    end
  end
  
  struct ENS_HealthKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :d, Google::Protobuf::StringValue, 1
      optional :st_val, HealthKind, 2
    end
  end
  
  struct Optional_SwitchingCapabilityKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :value, SwitchingCapabilityKind, 1
    end
  end
  
  struct ENS_SwitchingCapabilityKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :blk_ena, Google::Protobuf::BoolValue, 1
      optional :st_val, SwitchingCapabilityKind, 2
    end
  end
  
  struct OperationDCTE
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :rnd_dl_tmms, ControlING, 1
      optional :rtn_dl_tmms, ControlING, 2
      optional :rtn_rmp_tmms, ControlING, 3
    end
  end
  
  struct EnterServiceAPC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :enter_service_parameter, OperationDCTE, 1
      optional :hz_hi_lim, :float, 2
      optional :hz_lo_lim, :float, 3
      optional :rtn_srv_auto, :bool, 4
      optional :v_hi_lim, :float, 5
      optional :v_lo_lim, :float, 6
    end
  end
  
  struct ESS
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :conducting_equipment, ConductingEquipment, 1
    end
  end
  
  struct EventMessageInfo
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :message_info, MessageInfo, 1
    end
  end
  
  struct EventValue
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :identified_object, IdentifiedObject, 1
      optional :mod_blk, Google::Protobuf::BoolValue, 2
    end
  end
  
  struct ForecastValueSource
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :identified_object, IdentifiedObject, 1
    end
  end
  
  struct ForecastIED
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :forecast_value_source, ForecastValueSource, 1
      optional :source_application_id, :string, 2
      optional :source_date_time, :int64, 3
    end
  end
  
  struct ForecastValue
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :identified_object, IdentifiedObject, 1
    end
  end
  
  struct OperationDHFW
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :mod_ena, :bool, 1
      optional :opl_tmms_max, ClearingTime, 2
    end
  end
  
  struct OperationDLFW
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :mod_ena, :bool, 1
      optional :opl_tmms_max, ClearingTime, 2
    end
  end
  
  struct HzWPoint
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :deadband_hz_val, :float, 1
      optional :slope_val, :float, 2
    end
  end
  
  struct HzWAPC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :over_hz_w_pt, HzWPoint, 1
      optional :over_hz_w_parameter, OperationDHFW, 2
      optional :under_hz_w_pt, HzWPoint, 3
      optional :under_hz_w_parameter, OperationDLFW, 4
    end
  end
  
  struct StatusINS
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :q, Quality, 1
      optional :st_val, :int32, 2
      optional :t, Timestamp, 3
    end
  end
  
  struct IntegerEventAndStatusGGIO
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node, LogicalNode, 1
      optional :int_in, StatusINS, 2
      optional :phase, OptionalPhaseCodeKind, 3
    end
  end
  
  struct OperationDWMX
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :mod_ena, :bool, 1
    end
  end
  
  struct OperationDWMN
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :mod_ena, :bool, 1
    end
  end
  
  struct LimitWAPC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :max_lim_parameter, OperationDWMX, 1
      optional :min_lim_parameter, OperationDWMN, 2
      optional :w_max_spt_val, :float, 3
      optional :w_min_spt_val, :float, 4
    end
  end
  
  struct LogicalNodeForEventAndStatus
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node, LogicalNode, 1
      optional :beh, ENSBehaviourModeKind, 2
      optional :ee_health, ENSHealthKind, 3
      optional :hot_line_tag, StatusSPS, 4
      optional :remote_blk, StatusSPS, 5
    end
  end
  
  struct MeasurementValue
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :identified_object, IdentifiedObject, 1
    end
  end
  
  struct Meter
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :conducting_equipment, ConductingEquipment, 1
    end
  end
  
  struct NameplateValue
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :identified_object, IdentifiedObject, 1
      optional :model, Google::Protobuf::StringValue, 2
      optional :sernum, Google::Protobuf::StringValue, 3
      optional :sw_rev, Google::Protobuf::StringValue, 4
      optional :vendor, Google::Protobuf::StringValue, 5
    end
  end
  
  struct OperationDFPF
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :mod_ena, :bool, 1
      optional :p_f_ext_set, :bool, 2
      optional :p_f_gn_tgt_mx_val, :float, 3
    end
  end
  
  struct OperationDVAR
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :var_tgt_spt, :float, 1
    end
  end
  
  struct OperationDVVR
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :mod_ena, :bool, 1
      optional :opl_tmms_max, ClearingTime, 2
      optional :v_ref, :float, 3
      optional :v_ref_adj_ena, :bool, 4
      optional :v_ref_tmms, ControlING, 5
    end
  end
  
  struct OperationDVWC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :mod_ena, :bool, 1
      optional :opl_tmms_max, ClearingTime, 2
    end
  end
  
  struct OperationDWGC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :w_spt, :float, 1
    end
  end
  
  struct OperationDWVR
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :mod_ena, :bool, 1
    end
  end
  
  struct OptimizationMessageInfo
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :message_info, MessageInfo, 1
    end
  end
  
  struct PFSPC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :ctl_val, :bool, 1
      optional :p_f_parameter, OperationDFPF, 2
    end
  end
  
  struct PhaseAPC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :phs3, ControlAPC, 1
      optional :phs_a, ControlAPC, 2
      optional :phs_b, ControlAPC, 3
      optional :phs_c, ControlAPC, 4
    end
  end
  
  struct Optional_DbPosKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :value, DbPosKind, 1
    end
  end
  
  struct StatusDPS
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :q, Quality, 1
      optional :st_val, DbPosKind, 2
      optional :t, Timestamp, 3
    end
  end
  
  struct PhaseDPS
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :phs3, StatusDPS, 1
      optional :phs_a, StatusDPS, 2
      optional :phs_b, StatusDPS, 3
      optional :phs_c, StatusDPS, 4
    end
  end
  
  struct PhaseINS
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :phs3, StatusINS, 1
      optional :phs_a, StatusINS, 2
      optional :phs_b, StatusINS, 3
      optional :phs_c, StatusINS, 4
    end
  end
  
  struct PhaseISC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :phs3, ControlISC, 1
      optional :phs_a, ControlISC, 2
      optional :phs_b, ControlISC, 3
      optional :phs_c, ControlISC, 4
    end
  end
  
  struct ReadingMMTN
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node, LogicalNode, 1
      optional :dmd_v_ah, BCR, 2
      optional :dmd_v_arh, BCR, 3
      optional :dmd_wh, BCR, 4
      optional :sup_v_ah, BCR, 5
      optional :sup_v_arh, BCR, 6
      optional :sup_wh, BCR, 7
      optional :tot_v_ah, BCR, 8
      optional :tot_v_arh, BCR, 9
      optional :tot_wh, BCR, 10
    end
  end
  
  struct PhaseMMTN
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :phs_a, ReadingMMTN, 1
      optional :phs_ab, ReadingMMTN, 2
      optional :phs_b, ReadingMMTN, 3
      optional :phs_bc, ReadingMMTN, 4
      optional :phs_c, ReadingMMTN, 5
      optional :phs_ca, ReadingMMTN, 6
    end
  end
  
  struct Optional_RecloseActionKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :value, RecloseActionKind, 1
    end
  end
  
  struct PhaseRecloseAction
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :phs3, OptionalRecloseActionKind, 1
      optional :phs_a, OptionalRecloseActionKind, 2
      optional :phs_b, OptionalRecloseActionKind, 3
      optional :phs_c, OptionalRecloseActionKind, 4
    end
  end
  
  struct PhaseSPC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :phs3, ControlSPC, 1
      optional :phs_a, ControlSPC, 2
      optional :phs_b, ControlSPC, 3
      optional :phs_c, ControlSPC, 4
    end
  end
  
  struct PhaseSPS
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :phs3, StatusSPS, 1
      optional :phs_a, StatusSPS, 2
      optional :phs_b, StatusSPS, 3
      optional :phs_c, StatusSPS, 4
    end
  end
  
  struct PMG
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :net, MV, 1
      optional :phs_a, MV, 2
      optional :phs_b, MV, 3
      optional :phs_c, MV, 4
    end
  end
  
  struct RampRate
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :negative_reactive_power_kv_ar_per_min, Google::Protobuf::FloatValue, 1
      optional :negative_real_power_kw_per_min, Google::Protobuf::FloatValue, 2
      optional :positive_reactive_power_kv_ar_per_min, Google::Protobuf::FloatValue, 3
      optional :positive_real_power_kw_per_min, Google::Protobuf::FloatValue, 4
    end
  end
  
  struct ReadingMessageInfo
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :message_info, MessageInfo, 1
    end
  end
  
  struct ReadingMMTR
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node, LogicalNode, 1
      optional :dmd_v_ah, BCR, 2
      optional :dmd_v_arh, BCR, 3
      optional :dmd_wh, BCR, 4
      optional :sup_v_ah, BCR, 5
      optional :sup_v_arh, BCR, 6
      optional :sup_wh, BCR, 7
      optional :tot_v_ah, BCR, 8
      optional :tot_v_arh, BCR, 9
      optional :tot_wh, BCR, 10
    end
  end
  
  struct WYE
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :net, CMV, 1
      optional :neut, CMV, 2
      optional :phs_a, CMV, 3
      optional :phs_b, CMV, 4
      optional :phs_c, CMV, 5
      optional :res, CMV, 6
    end
  end
  
  struct ReadingMMXU
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node, LogicalNode, 1
      optional :a, WYE, 2
      optional :clc_mth, ENGCalcMethodKind, 3
      optional :hz, MV, 4
      optional :pf, WYE, 5
      optional :pf_sign, ENGPFSignKind, 6
      optional :ph_v, WYE, 7
      optional :ppv, DEL, 8
      optional :va, WYE, 9
      optional :v_ar, WYE, 10
      optional :w, WYE, 11
    end
  end
  
  struct SourceCapabilityConfiguration
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node, LogicalNode, 1
      optional :a_max, ASG, 2
      optional :va_max, ASG, 3
      optional :var_max_abs, ASG, 4
      optional :var_max_inj, ASG, 5
      optional :v_max, ASG, 6
      optional :v_min, ASG, 7
      optional :v_nom, ASG, 8
      optional :w_max, ASG, 9
      optional :w_ovr_ext, ASG, 10
      optional :w_ovr_ext_pf, ASG, 11
      optional :w_und_ext, ASG, 12
      optional :w_und_ext_pf, ASG, 13
    end
  end
  
  struct Optional_NorOpCatKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :value, NorOpCatKind, 1
    end
  end
  
  struct Optional_AbnOpCatKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :value, AbnOpCatKind, 1
    end
  end
  
  struct SourceCapabilityRatings
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node, LogicalNode, 1
      optional :abn_op_cat_rtg, AbnOpCatKind, 2
      optional :a_max_rtg, ASG, 3
      optional :freq_nom_rtg, ASG, 4
      optional :nor_op_cat_rtg, NorOpCatKind, 5
      optional :react_suscept_rtg, ASG, 6
      optional :va_max_rtg, ASG, 7
      optional :var_max_abs_rtg, ASG, 8
      optional :var_max_inj_rtg, ASG, 9
      optional :v_max_rtg, ASG, 10
      optional :v_min_rtg, ASG, 11
      optional :v_nom_rtg, ASG, 12
      optional :w_max_rtg, ASG, 13
      optional :w_ovr_ext_rtg, ASG, 14
      optional :w_ovr_ext_rtg_pf, ASG, 15
      optional :w_und_ext_rtg, ASG, 16
      optional :w_und_ext_rtg_pf, ASG, 17
    end
  end
  
  struct StatusAndEventXCBR
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node_for_event_and_status, LogicalNodeForEventAndStatus, 1
      optional :dynamic_test, ENSDynamicTestKind, 2
      optional :pos, PhaseDPS, 3
      optional :protection_pickup, ACD, 4
      optional :protection_mode, StatusINS, 5
      optional :reclose_enabled, PhaseSPS, 6
      optional :reclosing_action, PhaseRecloseAction, 7
    end
  end
  
  struct StatusINC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :q, Quality, 1
      optional :st_val, :int32, 2
      optional :t, Timestamp, 3
    end
  end
  
  struct StatusISC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :q, Quality, 1
      optional :st_val, :int32, 2
      optional :t, Timestamp, 3
    end
  end
  
  struct StatusMessageInfo
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :message_info, MessageInfo, 1
    end
  end
  
  struct StatusValue
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :identified_object, IdentifiedObject, 1
      optional :mod_blk, Google::Protobuf::BoolValue, 2
    end
  end
  
  struct VSS
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :q, Quality, 1
      optional :st_val, :string, 2
      optional :t, Timestamp, 3
    end
  end
  
  struct StringEventAndStatusGGIO
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node, LogicalNode, 1
      optional :phase, OptionalPhaseCodeKind, 2
      optional :str_in, VSS, 3
    end
  end
  
  struct SwitchPoint
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :pos, ControlDPC, 1
      optional :start_time, ControlTimestamp, 2
    end
  end
  
  struct SwitchCSG
    include ::Protobuf::Message
    
    contract_of "proto3" do
      repeated :crv_pts, SwitchPoint, 1
    end
  end
  
  struct TmHzPoint
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :hz_val, :float, 1
      optional :tm_val, ClearingTime, 2
    end
  end
  
  struct TmHzCSG
    include ::Protobuf::Message
    
    contract_of "proto3" do
      repeated :over_crv_pts, TmHzPoint, 1
      repeated :under_crv_pts, TmHzPoint, 2
    end
  end
  
  struct TmVoltPoint
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :tm_val, ClearingTime, 1
      optional :volt_val, :float, 2
    end
  end
  
  struct TmVoltCSG
    include ::Protobuf::Message
    
    contract_of "proto3" do
      repeated :over_crv_pts, TmVoltPoint, 1
      repeated :under_crv_pts, TmVoltPoint, 2
    end
  end
  
  struct VarSPC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :mod_ena, :bool, 1
      optional :var_parameter, OperationDVAR, 2
    end
  end
  
  struct VoltVarPoint
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :var_val, :float, 1
      optional :volt_val, :float, 2
    end
  end
  
  struct VoltVarCSG
    include ::Protobuf::Message
    
    contract_of "proto3" do
      repeated :crv_pts, VoltVarPoint, 1
      optional :v_var_parameter, OperationDVVR, 2
    end
  end
  
  struct VoltWPoint
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :volt_val, :float, 1
      optional :w_val, :float, 2
    end
  end
  
  struct VoltWCSG
    include ::Protobuf::Message
    
    contract_of "proto3" do
      repeated :crv_pts, VoltWPoint, 1
      optional :volt_w_parameter, OperationDVWC, 2
    end
  end
  
  struct VSC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :ctl_val, :string, 1
    end
  end
  
  struct WSPC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :mod_ena, :bool, 1
      optional :w_parameter, OperationDWGC, 2
    end
  end
  
  struct WVarPoint
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :var_val, :float, 1
      optional :w_val, :float, 2
    end
  end
  
  struct WVarCSG
    include ::Protobuf::Message
    
    contract_of "proto3" do
      repeated :crv_pts, WVarPoint, 1
      optional :w_var_parameter, OperationDWVR, 2
    end
  end
  
  struct Optional_AlrmKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :value, AlrmKind, 1
    end
  end
  
  struct Optional_ControlModeKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :value, ControlModeKind, 1
    end
  end
  
  struct Optional_DirectionModeKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :value, DirectionModeKind, 1
    end
  end
  
  struct Optional_GridConnectionStateKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :value, GridConnectionStateKind, 1
    end
  end
  
  struct Optional_OperatingStateKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :value, OperatingStateKind, 1
    end
  end
  
  struct Optional_ReactivePowerControlKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :value, ReactivePowerControlKind, 1
    end
  end
  
  struct Optional_RealPowerControlKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :value, RealPowerControlKind, 1
    end
  end
  
  struct Optional_StateKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :value, StateKind, 1
    end
  end
  
  struct Optional_VoltLimitModeKind
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :value, VoltLimitModeKind, 1
    end
  end
  end
