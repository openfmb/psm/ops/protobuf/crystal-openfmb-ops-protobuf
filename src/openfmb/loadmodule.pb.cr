## Generated from loadmodule/loadmodule.proto for loadmodule
require "protobuf"

require "./uml.pb.cr"
require "./wrappers.pb.cr"
require "./commonmodule.pb.cr"

module Loadmodule
  
  struct LoadPoint
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :ramp_rates, Commonmodule::RampRate, 1
      optional :reactive_pwr_set_point_enabled, Commonmodule::ControlSPC, 2
      optional :real_pwr_set_point_enabled, Commonmodule::ControlSPC, 3
      optional :reset, Commonmodule::ControlSPC, 4
      optional :state, Commonmodule::OptionalStateKind, 5
      optional :start_time, Commonmodule::ControlTimestamp, 6
    end
  end
  
  struct LoadCSG
    include ::Protobuf::Message
    
    contract_of "proto3" do
      repeated :crv_pts, LoadPoint, 1
    end
  end
  
  struct LoadControlScheduleFSCH
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :val_dcsg, LoadCSG, 1
    end
  end
  
  struct LoadControlFSCC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_fscc, Commonmodule::ControlFSCC, 1
      optional :load_control_schedule_fsch, LoadControlScheduleFSCH, 2
    end
  end
  
  struct LoadControl
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_value, Commonmodule::ControlValue, 1
      optional :check, Commonmodule::CheckConditions, 2
      optional :load_control_fscc, LoadControlFSCC, 3
    end
  end
  
  struct LoadControlProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_message_info, Commonmodule::ControlMessageInfo, 1
      optional :energy_consumer, Commonmodule::EnergyConsumer, 2
      optional :load_control, LoadControl, 3
    end
  end
  
  struct LoadPointStatus
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :ramp_rates, Commonmodule::RampRate, 1
      optional :reactive_pwr_set_point_enabled, Commonmodule::StatusSPS, 2
      optional :real_pwr_set_point_enabled, Commonmodule::StatusSPS, 3
      optional :reset, Commonmodule::StatusSPS, 4
      optional :state, Commonmodule::OptionalStateKind, 5
    end
  end
  
  struct LoadEventAndStatusZGLD
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node_for_event_and_status, Commonmodule::LogicalNodeForEventAndStatus, 1
      optional :dynamic_test, Commonmodule::ENSDynamicTestKind, 2
      optional :emg_stop, Commonmodule::StatusSPS, 3
      optional :point_status, LoadPointStatus, 4
    end
  end
  
  struct LoadEventZGLD
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :load_event_and_status_zgld, LoadEventAndStatusZGLD, 1
    end
  end
  
  struct LoadEvent
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :event_value, Commonmodule::EventValue, 1
      optional :load_event_zgld, LoadEventZGLD, 2
    end
  end
  
  struct LoadEventProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :event_message_info, Commonmodule::EventMessageInfo, 1
      optional :energy_consumer, Commonmodule::EnergyConsumer, 2
      optional :load_event, LoadEvent, 3
    end
  end
  
  struct LoadReading
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :conducting_equipment_terminal_reading, Commonmodule::ConductingEquipmentTerminalReading, 1
      optional :phase_mmtn, Commonmodule::PhaseMMTN, 2
      optional :reading_mmtr, Commonmodule::ReadingMMTR, 3
      optional :reading_mmxu, Commonmodule::ReadingMMXU, 4
    end
  end
  
  struct LoadReadingProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :reading_message_info, Commonmodule::ReadingMessageInfo, 1
      optional :energy_consumer, Commonmodule::EnergyConsumer, 2
      optional :load_reading, LoadReading, 3
    end
  end
  
  struct LoadStatusZGLD
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :load_event_and_status_zgld, LoadEventAndStatusZGLD, 1
    end
  end
  
  struct LoadStatus
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :status_value, Commonmodule::StatusValue, 1
      optional :is_uncontrollable, Google::Protobuf::BoolValue, 2
      optional :load_status_zgld, LoadStatusZGLD, 3
    end
  end
  
  struct LoadStatusProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :status_message_info, Commonmodule::StatusMessageInfo, 1
      optional :energy_consumer, Commonmodule::EnergyConsumer, 2
      optional :load_status, LoadStatus, 3
    end
  end
  end
