## Generated from reclosermodule/reclosermodule.proto for reclosermodule
require "protobuf"

require "./uml.pb.cr"
require "./wrappers.pb.cr"
require "./commonmodule.pb.cr"

module Reclosermodule
  
  struct RecloserDiscreteControlXCBR
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :discrete_control_xcbr, Commonmodule::DiscreteControlXCBR, 1
    end
  end
  
  struct RecloserDiscreteControl
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_value, Commonmodule::ControlValue, 1
      optional :check, Commonmodule::CheckConditions, 2
      optional :recloser_discrete_control_xcbr, RecloserDiscreteControlXCBR, 3
    end
  end
  
  struct Recloser
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :conducting_equipment, Commonmodule::ConductingEquipment, 1
      optional :normal_open, Google::Protobuf::BoolValue, 2
    end
  end
  
  struct RecloserDiscreteControlProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_message_info, Commonmodule::ControlMessageInfo, 1
      optional :recloser, Recloser, 2
      optional :recloser_discrete_control, RecloserDiscreteControl, 3
    end
  end
  
  struct RecloserEvent
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :event_value, Commonmodule::EventValue, 1
      optional :status_and_event_xcbr, Commonmodule::StatusAndEventXCBR, 2
    end
  end
  
  struct RecloserEventProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :event_message_info, Commonmodule::EventMessageInfo, 1
      optional :recloser, Recloser, 2
      optional :recloser_event, RecloserEvent, 3
    end
  end
  
  struct RecloserReading
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :conducting_equipment_terminal_reading, Commonmodule::ConductingEquipmentTerminalReading, 1
      optional :diff_reading_mmxu, Commonmodule::ReadingMMXU, 2
      optional :phase_mmtn, Commonmodule::PhaseMMTN, 3
      optional :reading_mmtr, Commonmodule::ReadingMMTR, 4
      optional :reading_mmxu, Commonmodule::ReadingMMXU, 5
    end
  end
  
  struct RecloserReadingProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :reading_message_info, Commonmodule::ReadingMessageInfo, 1
      optional :recloser, Recloser, 2
      repeated :recloser_reading, RecloserReading, 3
    end
  end
  
  struct RecloserStatus
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :status_value, Commonmodule::StatusValue, 1
      optional :status_and_event_xcbr, Commonmodule::StatusAndEventXCBR, 2
    end
  end
  
  struct RecloserStatusProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :status_message_info, Commonmodule::StatusMessageInfo, 1
      optional :recloser, Recloser, 2
      optional :recloser_status, RecloserStatus, 3
    end
  end
  end
