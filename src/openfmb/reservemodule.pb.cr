## Generated from reservemodule/reservemodule.proto for reservemodule
require "protobuf"

require "./uml.pb.cr"
require "./commonmodule.pb.cr"

module Reservemodule
  
  struct ReserveMargin
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node, Commonmodule::LogicalNode, 1
      optional :a, Commonmodule::PMG, 2
      optional :va, Commonmodule::PMG, 3
      optional :v_ar, Commonmodule::PMG, 4
      optional :w, Commonmodule::PMG, 5
    end
  end
  
  struct ReserveAvailability
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :incremental_margin, ReserveMargin, 1
      optional :margin, ReserveMargin, 2
      optional :standby_margin, ReserveMargin, 3
    end
  end
  
  struct AllocatedMargin
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :request_id, :string, 1
      optional :allocated_margin, ReserveMargin, 2
      optional :allocated_standby_margin, ReserveMargin, 3
    end
  end
  
  struct ReserveAvailabilityProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_message_info, Commonmodule::ControlMessageInfo, 1
      optional :allocated_margin, AllocatedMargin, 2
      optional :requester_circuit_segment_service, Commonmodule::ApplicationSystem, 3
      optional :reserve_availability, ReserveAvailability, 4
      optional :responder_circuit_segment_service, Commonmodule::ApplicationSystem, 5
      optional :tie_point, Commonmodule::ConductingEquipment, 6
    end
  end
  
  struct ReserveRequest
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :request_id, :string, 1
      optional :margin, ReserveMargin, 2
      optional :standby_margin, ReserveMargin, 3
    end
  end
  
  struct ReserveRequestProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_message_info, Commonmodule::ControlMessageInfo, 1
      optional :requester_circuit_segment_service, Commonmodule::ApplicationSystem, 2
      optional :reserve_request, ReserveRequest, 3
      optional :responder_circuit_segment_service, Commonmodule::ApplicationSystem, 4
      optional :tie_point, Commonmodule::ConductingEquipment, 5
    end
  end
  end
