## Generated from essmodule/essmodule.proto for essmodule
require "protobuf"

require "./uml.pb.cr"
require "./wrappers.pb.cr"
require "./commonmodule.pb.cr"

module Essmodule
  
  struct ESSCapabilityConfiguration
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :source_capability_configuration, Commonmodule::SourceCapabilityConfiguration, 1
      optional :va_cha_rte_max, Commonmodule::ASG, 2
      optional :va_dis_cha_rte_max, Commonmodule::ASG, 3
      optional :w_cha_rte_max, Commonmodule::ASG, 4
      optional :w_dis_cha_rte_max, Commonmodule::ASG, 5
    end
  end
  
  struct ESSCapabilityOverride
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :nameplate_value, Commonmodule::NameplateValue, 1
      optional :ess_capability_configuration, ESSCapabilityConfiguration, 2
    end
  end
  
  struct ESSCapabilityOverrideProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :capability_message_info, Commonmodule::CapabilityMessageInfo, 1
      optional :ess, Commonmodule::ESS, 2
      optional :ess_capability_override, ESSCapabilityOverride, 3
    end
  end
  
  struct ESSCapabilityRatings
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :source_capability_ratings, Commonmodule::SourceCapabilityRatings, 1
      optional :va_cha_rte_max_rtg, Commonmodule::ASG, 2
      optional :va_dis_cha_rte_max_rtg, Commonmodule::ASG, 3
      optional :w_cha_rte_max_rtg, Commonmodule::ASG, 4
      optional :w_dis_cha_rte_max_rtg, Commonmodule::ASG, 5
      optional :wh_rtg, Commonmodule::ASG, 6
    end
  end
  
  struct ESSCapability
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :nameplate_value, Commonmodule::NameplateValue, 1
      optional :ess_capability_ratings, ESSCapabilityRatings, 2
      optional :ess_capability_configuration, ESSCapabilityConfiguration, 3
    end
  end
  
  struct ESSCapabilityProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :capability_message_info, Commonmodule::CapabilityMessageInfo, 1
      optional :ess, Commonmodule::ESS, 2
      optional :ess_capability, ESSCapability, 3
    end
  end
  
  struct FrequencyRegulation
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :frequency_dead_band_minus, Google::Protobuf::FloatValue, 1
      optional :frequency_dead_band_plus, Google::Protobuf::FloatValue, 2
      optional :frequency_regulation_ctl, Google::Protobuf::BoolValue, 3
      optional :frequency_set_point, Google::Protobuf::FloatValue, 4
      optional :grid_frequency_stable_band_minus, Google::Protobuf::FloatValue, 5
      optional :grid_frequency_stable_band_plus, Google::Protobuf::FloatValue, 6
      optional :over_frequency_droop, Google::Protobuf::FloatValue, 7
      optional :under_frequency_droop, Google::Protobuf::FloatValue, 8
    end
  end
  
  struct PeakShaving
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :base_shaving_limit, Google::Protobuf::FloatValue, 1
      optional :peak_shaving_ctl, Google::Protobuf::BoolValue, 2
      optional :peak_shaving_limit, Google::Protobuf::FloatValue, 3
      optional :soc_management_allowed_high_limit, Google::Protobuf::FloatValue, 4
      optional :soc_management_allowed_low_limit, Google::Protobuf::FloatValue, 5
    end
  end
  
  struct SocLimit
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :soc_high_limit, Google::Protobuf::FloatValue, 1
      optional :soc_high_limit_hysteresis, Google::Protobuf::FloatValue, 2
      optional :soc_limit_ctl, Google::Protobuf::BoolValue, 3
      optional :soc_low_limit, Google::Protobuf::FloatValue, 4
      optional :soc_low_limit_hysteresis, Google::Protobuf::FloatValue, 5
    end
  end
  
  struct SOCManagement
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :soc_dead_band_minus, Google::Protobuf::FloatValue, 1
      optional :soc_dead_band_plus, Google::Protobuf::FloatValue, 2
      optional :soc_management_ctl, Google::Protobuf::BoolValue, 3
      optional :soc_power_set_point, Google::Protobuf::FloatValue, 4
      optional :soc_set_point, Google::Protobuf::FloatValue, 5
    end
  end
  
  struct VoltageRegulation
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :over_voltage_droop, Google::Protobuf::FloatValue, 1
      optional :under_voltage_droop, Google::Protobuf::FloatValue, 2
      optional :voltage_dead_band_minus, Google::Protobuf::FloatValue, 3
      optional :voltage_dead_band_plus, Google::Protobuf::FloatValue, 4
      optional :voltage_set_point, Google::Protobuf::FloatValue, 5
    end
  end
  
  struct VoltageDroop
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :voltage_droop_ctl, Google::Protobuf::BoolValue, 1
      optional :voltage_regulation, VoltageRegulation, 2
    end
  end
  
  struct VoltagePI
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :voltage_pi_ctl, Google::Protobuf::BoolValue, 1
      optional :voltage_regulation, VoltageRegulation, 2
    end
  end
  
  struct CapacityFirming
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :capacity_firming_ctl, Google::Protobuf::BoolValue, 1
      optional :limit_negative_dp_dt, Google::Protobuf::FloatValue, 2
      optional :limit_positive_dp_dt, Google::Protobuf::FloatValue, 3
    end
  end
  
  struct ESSFunction
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :capacity_firming, CapacityFirming, 1
      optional :frequency_regulation, FrequencyRegulation, 2
      optional :peak_shaving, PeakShaving, 3
      optional :soc_limit, SocLimit, 4
      optional :soc_management, SOCManagement, 5
      optional :voltage_droop, VoltageDroop, 6
      optional :voltage_pi, VoltagePI, 7
    end
  end
  
  struct ESSPoint
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :black_start_enabled, Commonmodule::ControlSPC, 1
      optional :function, ESSFunction, 3
      optional :mode, Commonmodule::ENGGridConnectModeKind, 4
      optional :ramp_rates, Commonmodule::RampRate, 7
      optional :reset, Commonmodule::ControlSPC, 10
      optional :state, Commonmodule::OptionalStateKind, 11
      optional :trans_to_islnd_on_grid_loss_enabled, Commonmodule::ControlSPC, 13
      optional :enter_service_operation, Commonmodule::EnterServiceAPC, 16
      optional :hz_w_operation, Commonmodule::HzWAPC, 17
      optional :limit_w_operation, Commonmodule::LimitWAPC, 18
      optional :p_f_operation, Commonmodule::PFSPC, 19
      optional :tm_hz_trip_operation, Commonmodule::TmHzCSG, 20
      optional :tm_volt_trip_operation, Commonmodule::TmVoltCSG, 21
      optional :v_ar_operation, Commonmodule::VarSPC, 22
      optional :volt_var_operation, Commonmodule::VoltVarCSG, 23
      optional :volt_w_operation, Commonmodule::VoltWCSG, 24
      optional :w_var_operation, Commonmodule::WVarCSG, 25
      optional :w_operation, Commonmodule::WSPC, 26
    end
  end
  
  struct ESSCurvePoint
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control, ESSPoint, 1
      optional :start_time, Commonmodule::ControlTimestamp, 2
    end
  end
  
  struct ESSCSG
    include ::Protobuf::Message
    
    contract_of "proto3" do
      repeated :crv_pts, ESSCurvePoint, 1
    end
  end
  
  struct ESSControlScheduleFSCH
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :val_dcsg, ESSCSG, 1
    end
  end
  
  struct EssControlFSCC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_fscc, Commonmodule::ControlFSCC, 1
      optional :ess_control_schedule_fsch, ESSControlScheduleFSCH, 2
    end
  end
  
  struct ESSControl
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_value, Commonmodule::ControlValue, 1
      optional :check, Commonmodule::CheckConditions, 2
      optional :ess_control_fscc, EssControlFSCC, 3
    end
  end
  
  struct ESSControlProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_message_info, Commonmodule::ControlMessageInfo, 1
      optional :ess, Commonmodule::ESS, 2
      optional :ess_control, ESSControl, 3
    end
  end
  
  struct ESSDiscreteControlDBAT
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node_for_control, Commonmodule::LogicalNodeForControl, 1
      optional :control, ESSPoint, 2
    end
  end
  
  struct ESSDiscreteControl
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_value, Commonmodule::ControlValue, 1
      optional :check, Commonmodule::CheckConditions, 2
      optional :ess_discrete_control_dbat, ESSDiscreteControlDBAT, 3
    end
  end
  
  struct ESSDiscreteControlProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_message_info, Commonmodule::ControlMessageInfo, 1
      optional :ess, Commonmodule::ESS, 2
      optional :ess_discrete_control, ESSDiscreteControl, 3
    end
  end
  
  struct EssEventZBAT
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node_for_event_and_status, Commonmodule::LogicalNodeForEventAndStatus, 1
      optional :bat_hi, Commonmodule::StatusSPS, 2
      optional :bat_lo, Commonmodule::StatusSPS, 3
      optional :bat_st, Commonmodule::StatusSPS, 4
      optional :soc, Commonmodule::MV, 5
      optional :stdby, Commonmodule::StatusSPS, 6
      optional :so_h, Commonmodule::MV, 7
      optional :wh_avail, Commonmodule::MV, 8
    end
  end
  
  struct ESSPointStatus
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :black_start_enabled, Commonmodule::StatusSPS, 1
      optional :frequency_set_point_enabled, Commonmodule::StatusSPS, 2
      optional :function, ESSFunction, 3
      optional :mode, Commonmodule::ENGGridConnectModeKind, 4
      optional :pct_hz_droop, Google::Protobuf::FloatValue, 5
      optional :pct_v_droop, Google::Protobuf::FloatValue, 6
      optional :ramp_rates, Commonmodule::RampRate, 7
      optional :reactive_pwr_set_point_enabled, Commonmodule::StatusSPS, 8
      optional :real_pwr_set_point_enabled, Commonmodule::StatusSPS, 9
      optional :state, Commonmodule::OptionalStateKind, 10
      optional :sync_back_to_grid, Commonmodule::StatusSPS, 11
      optional :trans_to_islnd_on_grid_loss_enabled, Commonmodule::StatusSPS, 12
      optional :voltage_set_point_enabled, Commonmodule::StatusSPS, 13
      optional :enter_service_operation, Commonmodule::EnterServiceAPC, 14
      optional :hz_w_operation, Commonmodule::HzWAPC, 15
      optional :limit_w_operation, Commonmodule::LimitWAPC, 16
      optional :p_f_operation, Commonmodule::PFSPC, 17
      optional :tm_hz_trip_operation, Commonmodule::TmHzCSG, 18
      optional :tm_volt_trip_operation, Commonmodule::TmVoltCSG, 19
      optional :v_ar_operation, Commonmodule::VarSPC, 20
      optional :volt_var_operation, Commonmodule::VoltVarCSG, 21
      optional :volt_w_operation, Commonmodule::VoltWCSG, 22
      optional :w_var_operation, Commonmodule::WVarCSG, 23
    end
  end
  
  struct ESSEventAndStatusZGEN
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node_for_event_and_status, Commonmodule::LogicalNodeForEventAndStatus, 1
      optional :aux_pwr_st, Commonmodule::StatusSPS, 2
      optional :dynamic_test, Commonmodule::ENSDynamicTestKind, 3
      optional :emg_stop, Commonmodule::StatusSPS, 4
      optional :gn_syn_st, Commonmodule::StatusSPS, 5
      optional :point_status, ESSPointStatus, 6
    end
  end
  
  struct ESSEventZGEN
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :e_ss_event_and_status_zgen, ESSEventAndStatusZGEN, 1
    end
  end
  
  struct ESSEvent
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :event_value, Commonmodule::EventValue, 1
      optional :ess_event_zbat, EssEventZBAT, 2
      optional :ess_event_zgen, ESSEventZGEN, 3
    end
  end
  
  struct ESSEventProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :event_message_info, Commonmodule::EventMessageInfo, 1
      optional :ess, Commonmodule::ESS, 2
      optional :ess_event, ESSEvent, 3
    end
  end
  
  struct ESSReading
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :conducting_equipment_terminal_reading, Commonmodule::ConductingEquipmentTerminalReading, 1
      optional :phase_mmtn, Commonmodule::PhaseMMTN, 2
      optional :reading_mmtr, Commonmodule::ReadingMMTR, 3
      optional :reading_mmxu, Commonmodule::ReadingMMXU, 4
    end
  end
  
  struct ESSReadingProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :reading_message_info, Commonmodule::ReadingMessageInfo, 1
      optional :ess, Commonmodule::ESS, 2
      optional :ess_reading, ESSReading, 3
    end
  end
  
  struct EssStatusZBAT
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node_for_event_and_status, Commonmodule::LogicalNodeForEventAndStatus, 1
      optional :bat_st, Commonmodule::StatusSPS, 2
      optional :gri_mod, Commonmodule::ENGGridConnectModeKind, 3
      optional :soc, Commonmodule::MV, 4
      optional :stdby, Commonmodule::StatusSPS, 5
      optional :so_h, Commonmodule::MV, 6
      optional :wh_avail, Commonmodule::MV, 7
    end
  end
  
  struct ESSStatusZGEN
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :e_ss_event_and_status_zgen, ESSEventAndStatusZGEN, 1
    end
  end
  
  struct ESSStatus
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :status_value, Commonmodule::StatusValue, 1
      optional :ess_status_zbat, EssStatusZBAT, 2
      optional :ess_status_zgen, ESSStatusZGEN, 3
    end
  end
  
  struct ESSStatusProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :status_message_info, Commonmodule::StatusMessageInfo, 1
      optional :ess, Commonmodule::ESS, 2
      optional :ess_status, ESSStatus, 3
    end
  end
  end
