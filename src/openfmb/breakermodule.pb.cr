## Generated from breakermodule/breakermodule.proto for breakermodule
require "protobuf"

require "./uml.pb.cr"
require "./commonmodule.pb.cr"

module Breakermodule
  
  struct BreakerDiscreteControlXCBR
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :discrete_control_xcbr, Commonmodule::DiscreteControlXCBR, 1
    end
  end
  
  struct BreakerDiscreteControl
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_value, Commonmodule::ControlValue, 1
      optional :check, Commonmodule::CheckConditions, 2
      optional :breaker_discrete_control_xcbr, BreakerDiscreteControlXCBR, 3
    end
  end
  
  struct Breaker
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :conducting_equipment, Commonmodule::ConductingEquipment, 1
    end
  end
  
  struct BreakerDiscreteControlProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_message_info, Commonmodule::ControlMessageInfo, 1
      optional :breaker, Breaker, 2
      optional :breaker_discrete_control, BreakerDiscreteControl, 3
    end
  end
  
  struct BreakerEvent
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :event_value, Commonmodule::EventValue, 1
      optional :status_and_event_xcbr, Commonmodule::StatusAndEventXCBR, 2
    end
  end
  
  struct BreakerEventProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :event_message_info, Commonmodule::EventMessageInfo, 1
      optional :breaker, Breaker, 2
      optional :breaker_event, BreakerEvent, 3
    end
  end
  
  struct BreakerReading
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :conducting_equipment_terminal_reading, Commonmodule::ConductingEquipmentTerminalReading, 1
      optional :diff_reading_mmxu, Commonmodule::ReadingMMXU, 2
      optional :phase_mmtn, Commonmodule::PhaseMMTN, 3
      optional :reading_mmtr, Commonmodule::ReadingMMTR, 4
      optional :reading_mmxu, Commonmodule::ReadingMMXU, 5
    end
  end
  
  struct BreakerReadingProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :reading_message_info, Commonmodule::ReadingMessageInfo, 1
      optional :breaker, Breaker, 2
      repeated :breaker_reading, BreakerReading, 3
    end
  end
  
  struct BreakerStatus
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :status_value, Commonmodule::StatusValue, 1
      optional :status_and_event_xcbr, Commonmodule::StatusAndEventXCBR, 2
    end
  end
  
  struct BreakerStatusProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :status_message_info, Commonmodule::StatusMessageInfo, 1
      optional :breaker, Breaker, 2
      optional :breaker_status, BreakerStatus, 3
    end
  end
  end
