## Generated from resourcemodule/resourcemodule.proto for resourcemodule
require "protobuf"

require "./uml.pb.cr"
require "./commonmodule.pb.cr"

module Resourcemodule
  
  struct BooleanControlGGIO
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node, Commonmodule::LogicalNode, 1
      optional :phase, Commonmodule::OptionalPhaseCodeKind, 2
      optional :spcso, Commonmodule::ControlSPC, 3
    end
  end
  
  struct IntegerControlGGIO
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node, Commonmodule::LogicalNode, 1
      optional :iscso, Commonmodule::ControlINC, 2
      optional :phase, Commonmodule::OptionalPhaseCodeKind, 3
    end
  end
  
  struct StringControlGGIO
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node, Commonmodule::LogicalNode, 1
      optional :phase, Commonmodule::OptionalPhaseCodeKind, 2
      optional :str_out, Commonmodule::VSC, 3
    end
  end
  
  struct AnalogControlGGIO
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node, Commonmodule::LogicalNode, 1
      optional :an_out, Commonmodule::ControlAPC, 2
      optional :phase, Commonmodule::OptionalPhaseCodeKind, 3
    end
  end
  
  struct ResourceDiscreteControl
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :identified_object, Commonmodule::IdentifiedObject, 1
      optional :check, Commonmodule::CheckConditions, 2
      repeated :analog_control_ggio, AnalogControlGGIO, 3
      repeated :boolean_control_ggio, BooleanControlGGIO, 4
      repeated :integer_control_ggio, IntegerControlGGIO, 5
      repeated :string_control_ggio, StringControlGGIO, 6
    end
  end
  
  struct ResourceDiscreteControlProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_message_info, Commonmodule::ControlMessageInfo, 1
      optional :conducting_equipment, Commonmodule::ConductingEquipment, 2
      optional :resource_discrete_control, ResourceDiscreteControl, 3
    end
  end
  
  struct ResourceReading
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :conducting_equipment_terminal_reading, Commonmodule::ConductingEquipmentTerminalReading, 1
      optional :phase_mmtn, Commonmodule::PhaseMMTN, 2
      optional :reading_mmtr, Commonmodule::ReadingMMTR, 3
      optional :reading_mmxu, Commonmodule::ReadingMMXU, 4
    end
  end
  
  struct ResourceReadingProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :reading_message_info, Commonmodule::ReadingMessageInfo, 1
      optional :conducting_equipment, Commonmodule::ConductingEquipment, 2
      optional :resource_reading, ResourceReading, 3
    end
  end
  
  struct ResourceEvent
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :identified_object, Commonmodule::IdentifiedObject, 1
      repeated :analog_event_and_status_ggio, Commonmodule::AnalogEventAndStatusGGIO, 2
      repeated :boolean_event_and_status_ggio, Commonmodule::BooleanEventAndStatusGGIO, 3
      repeated :integer_event_and_status_ggio, Commonmodule::IntegerEventAndStatusGGIO, 4
      repeated :string_event_and_status_ggio, Commonmodule::StringEventAndStatusGGIO, 5
    end
  end
  
  struct ResourceEventProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :event_message_info, Commonmodule::EventMessageInfo, 1
      optional :conducting_equipment, Commonmodule::ConductingEquipment, 2
      optional :resource_event, ResourceEvent, 3
    end
  end
  
  struct ResourceStatus
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :identified_object, Commonmodule::IdentifiedObject, 1
      repeated :analog_event_and_status_ggio, Commonmodule::AnalogEventAndStatusGGIO, 2
      repeated :boolean_event_and_status_ggio, Commonmodule::BooleanEventAndStatusGGIO, 3
      repeated :integer_event_and_status_ggio, Commonmodule::IntegerEventAndStatusGGIO, 4
      repeated :string_event_and_status_ggio, Commonmodule::StringEventAndStatusGGIO, 5
    end
  end
  
  struct ResourceStatusProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :status_message_info, Commonmodule::StatusMessageInfo, 1
      optional :conducting_equipment, Commonmodule::ConductingEquipment, 2
      optional :resource_status, ResourceStatus, 3
    end
  end
  end
