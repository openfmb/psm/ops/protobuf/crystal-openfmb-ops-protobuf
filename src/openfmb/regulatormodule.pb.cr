## Generated from regulatormodule/regulatormodule.proto for regulatormodule
require "protobuf"

require "./uml.pb.cr"
require "./commonmodule.pb.cr"

module Regulatormodule
  
  struct DirectionalATCC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :bnd_wid, Commonmodule::PhaseAPC, 1
      optional :ctl_dl_tmms, Commonmodule::PhaseISC, 2
      optional :ldcr, Commonmodule::PhaseAPC, 3
      optional :ldcx, Commonmodule::PhaseAPC, 4
      optional :vol_spt, Commonmodule::PhaseAPC, 5
      optional :voltage_set_point_enabled, Commonmodule::PhaseDPC, 6
    end
  end
  
  struct RegulatorControlATCC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node_for_control, Commonmodule::LogicalNodeForControl, 1
      optional :dir_fwd, DirectionalATCC, 2
      optional :dir_mode, Commonmodule::OptionalDirectionModeKind, 3
      optional :dir_rev, DirectionalATCC, 4
      optional :dir_thd, Commonmodule::PhaseAPC, 5
      optional :par_op, Commonmodule::PhaseSPC, 6
      optional :ramp_rates, Commonmodule::RampRate, 7
      optional :state, Commonmodule::OptionalStateKind, 8
      optional :tap_op_l, Commonmodule::PhaseSPC, 9
      optional :tap_op_r, Commonmodule::PhaseSPC, 10
      optional :vol_lmt_hi, Commonmodule::PhaseAPC, 11
      optional :vol_lmt_lo, Commonmodule::PhaseAPC, 12
      optional :vol_lmt_mode, Commonmodule::OptionalVoltLimitModeKind, 13
    end
  end
  
  struct RegulatorPoint
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control, RegulatorControlATCC, 1
      optional :start_time, Commonmodule::Timestamp, 8
    end
  end
  
  struct RegulatorCSG
    include ::Protobuf::Message
    
    contract_of "proto3" do
      repeated :crv_pts, RegulatorPoint, 1
    end
  end
  
  struct RegulatorControlScheduleFSCH
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :val_dcsg, RegulatorCSG, 1
    end
  end
  
  struct RegulatorControlFSCC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_fscc, Commonmodule::ControlFSCC, 1
      optional :regulator_control_schedule_fsch, RegulatorControlScheduleFSCH, 2
    end
  end
  
  struct RegulatorControl
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_value, Commonmodule::ControlValue, 1
      optional :check, Commonmodule::CheckConditions, 2
      optional :regulator_control_fscc, RegulatorControlFSCC, 3
    end
  end
  
  struct RegulatorSystem
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :conducting_equipment, Commonmodule::ConductingEquipment, 1
    end
  end
  
  struct RegulatorControlProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_message_info, Commonmodule::ControlMessageInfo, 1
      optional :regulator_control, RegulatorControl, 2
      optional :regulator_system, RegulatorSystem, 3
    end
  end
  
  struct RegulatorDiscreteControl
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_value, Commonmodule::ControlValue, 1
      optional :check, Commonmodule::CheckConditions, 2
      optional :regulator_control_atcc, RegulatorControlATCC, 3
    end
  end
  
  struct RegulatorDiscreteControlProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_message_info, Commonmodule::ControlMessageInfo, 1
      optional :regulator_discrete_control, RegulatorDiscreteControl, 2
      optional :regulator_system, RegulatorSystem, 3
    end
  end
  
  struct RegulatorEventAndStatusATCC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :bnd_ctr, Commonmodule::ASG, 1
      optional :bnd_wid, Commonmodule::ASG, 2
      optional :bnd_wid_hi, Commonmodule::PhaseSPS, 3
      optional :bnd_wid_lo, Commonmodule::PhaseSPS, 4
      optional :dir_ctl_rev, Commonmodule::PhaseSPS, 5
      optional :dir_indt, Commonmodule::PhaseSPS, 6
      optional :dir_rev, Commonmodule::PhaseSPS, 7
      optional :ldcr, Commonmodule::ASG, 8
      optional :ldcx, Commonmodule::ASG, 9
      optional :par_op, Commonmodule::StatusSPS, 10
      optional :ramp_rates, Commonmodule::RampRate, 11
      optional :state, Commonmodule::OptionalStateKind, 12
      optional :st_dl_tmms, Commonmodule::StatusINC, 13
      optional :tap_op_err, Commonmodule::StatusSPS, 14
      optional :tap_pos, Commonmodule::PhaseINS, 15
      optional :vol_lmt_hi, Commonmodule::PhaseSPS, 16
      optional :vol_lmt_lo, Commonmodule::PhaseSPS, 17
      optional :vol_spt, Commonmodule::PhaseAPC, 18
      optional :voltage_set_point_enabled, Commonmodule::StatusSPS, 19
    end
  end
  
  struct RegulatorEventAndStatusANCR
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node_for_event_and_status, Commonmodule::LogicalNodeForEventAndStatus, 1
      optional :dynamic_test, Commonmodule::ENSDynamicTestKind, 2
      optional :point_status, RegulatorEventAndStatusATCC, 3
    end
  end
  
  struct RegulatorEvent
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :event_value, Commonmodule::EventValue, 1
      optional :regulator_event_and_status_ancr, RegulatorEventAndStatusANCR, 2
    end
  end
  
  struct RegulatorEventProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :event_message_info, Commonmodule::EventMessageInfo, 1
      optional :regulator_event, RegulatorEvent, 2
      optional :regulator_system, RegulatorSystem, 3
    end
  end
  
  struct RegulatorReading
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :conducting_equipment_terminal_reading, Commonmodule::ConductingEquipmentTerminalReading, 1
      optional :phase_mmtn, Commonmodule::PhaseMMTN, 2
      optional :reading_mmtr, Commonmodule::ReadingMMTR, 3
      optional :reading_mmxu, Commonmodule::ReadingMMXU, 4
      optional :secondary_reading_mmxu, Commonmodule::ReadingMMXU, 5
    end
  end
  
  struct RegulatorReadingProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :reading_message_info, Commonmodule::ReadingMessageInfo, 1
      repeated :regulator_reading, RegulatorReading, 2
      optional :regulator_system, RegulatorSystem, 3
    end
  end
  
  struct RegulatorStatus
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :status_value, Commonmodule::StatusValue, 1
      optional :regulator_event_and_status_ancr, RegulatorEventAndStatusANCR, 2
    end
  end
  
  struct RegulatorStatusProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :status_message_info, Commonmodule::StatusMessageInfo, 1
      optional :regulator_status, RegulatorStatus, 2
      optional :regulator_system, RegulatorSystem, 3
    end
  end
  end
