## Generated from generationmodule/generationmodule.proto for generationmodule
require "protobuf"

require "./uml.pb.cr"
require "./wrappers.pb.cr"
require "./commonmodule.pb.cr"

module Generationmodule
  
  struct GeneratingUnit
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :conducting_equipment, Commonmodule::ConductingEquipment, 1
      optional :max_operating_p, Commonmodule::ActivePower, 2
    end
  end
  
  struct GenerationCapabilityConfiguration
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :source_capability_configuration, Commonmodule::SourceCapabilityConfiguration, 1
    end
  end
  
  struct GenerationCapabilityOverride
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :identified_object, Commonmodule::IdentifiedObject, 1
      optional :generation_capability_configuration, GenerationCapabilityConfiguration, 2
    end
  end
  
  struct GenerationCapabilityOverrideProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :capability_message_info, Commonmodule::CapabilityMessageInfo, 1
      optional :generation_capability_override, GenerationCapabilityOverride, 2
      optional :generating_unit, GeneratingUnit, 3
    end
  end
  
  struct GenerationCapabilityRatings
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :source_capability_ratings, Commonmodule::SourceCapabilityRatings, 1
    end
  end
  
  struct GenerationCapability
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :nameplate_value, Commonmodule::NameplateValue, 1
      optional :generation_capability_ratings, GenerationCapabilityRatings, 2
      optional :generation_capability_configuration, GenerationCapabilityConfiguration, 3
    end
  end
  
  struct GenerationCapabilityProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :capability_message_info, Commonmodule::CapabilityMessageInfo, 1
      optional :generation_capability, GenerationCapability, 2
      optional :generating_unit, GeneratingUnit, 3
    end
  end
  
  struct GenerationPoint
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :black_start_enabled, Commonmodule::ControlSPC, 1
      optional :frequency_set_point_enabled, Commonmodule::ControlSPC, 2
      optional :pct_hz_droop, Google::Protobuf::FloatValue, 3
      optional :pct_v_droop, Google::Protobuf::FloatValue, 4
      optional :ramp_rates, Commonmodule::RampRate, 5
      optional :reactive_pwr_set_point_enabled, Commonmodule::ControlSPC, 6
      optional :real_pwr_set_point_enabled, Commonmodule::ControlSPC, 7
      optional :reset, Commonmodule::ControlSPC, 8
      optional :state, Commonmodule::OptionalStateKind, 9
      optional :sync_back_to_grid, Commonmodule::ControlSPC, 10
      optional :trans_to_islnd_on_grid_loss_enabled, Commonmodule::ControlSPC, 11
      optional :voltage_set_point_enabled, Commonmodule::ControlSPC, 12
      optional :start_time, Commonmodule::ControlTimestamp, 13
      optional :enter_service_operation, Commonmodule::EnterServiceAPC, 14
      optional :hz_w_operation, Commonmodule::HzWAPC, 15
      optional :limit_w_operation, Commonmodule::LimitWAPC, 16
      optional :p_f_operation, Commonmodule::PFSPC, 17
      optional :tm_hz_trip_operation, Commonmodule::TmHzCSG, 18
      optional :tm_volt_trip_operation, Commonmodule::TmVoltCSG, 19
      optional :v_ar_operation, Commonmodule::VarSPC, 20
      optional :volt_var_operation, Commonmodule::VoltVarCSG, 21
      optional :volt_w_operation, Commonmodule::VoltWCSG, 22
      optional :w_var_operation, Commonmodule::WVarCSG, 23
    end
  end
  
  struct GenerationCSG
    include ::Protobuf::Message
    
    contract_of "proto3" do
      repeated :crv_pts, GenerationPoint, 1
    end
  end
  
  struct GenerationControlScheduleFSCH
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :val_dcsg, GenerationCSG, 1
    end
  end
  
  struct GenerationControlFSCC
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_fscc, Commonmodule::ControlFSCC, 1
      optional :generation_control_schedule_fsch, GenerationControlScheduleFSCH, 2
    end
  end
  
  struct GenerationControl
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_value, Commonmodule::ControlValue, 1
      optional :check, Commonmodule::CheckConditions, 2
      optional :generation_control_fscc, GenerationControlFSCC, 3
    end
  end
  
  struct GenerationControlProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_message_info, Commonmodule::ControlMessageInfo, 1
      optional :generating_unit, GeneratingUnit, 2
      optional :generation_control, GenerationControl, 3
    end
  end
  
  struct DroopParameter
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :slope, Google::Protobuf::FloatValue, 1
      optional :unloaded_offset, Google::Protobuf::FloatValue, 2
    end
  end
  
  struct RealPowerControl
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :droop_setpoint, DroopParameter, 1
      optional :isochronous_setpoint, Google::Protobuf::FloatValue, 2
      optional :real_power_control_mode, Commonmodule::OptionalRealPowerControlKind, 3
      optional :real_power_setpoint, Google::Protobuf::FloatValue, 4
    end
  end
  
  struct ReactivePowerControl
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :droop_setpoint, DroopParameter, 1
      optional :power_factor_setpoint, Google::Protobuf::FloatValue, 2
      optional :reactive_power_control_mode, Commonmodule::OptionalReactivePowerControlKind, 3
      optional :reactive_power_setpoint, Google::Protobuf::FloatValue, 4
      optional :voltage_setpoint, Google::Protobuf::FloatValue, 5
    end
  end
  
  struct GenerationDiscreteControl
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_value, Commonmodule::ControlValue, 1
      optional :check, Commonmodule::CheckConditions, 2
      optional :reactive_power_control, ReactivePowerControl, 3
      optional :real_power_control, RealPowerControl, 4
    end
  end
  
  struct GenerationDiscreteControlProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :control_message_info, Commonmodule::ControlMessageInfo, 1
      optional :generating_unit, GeneratingUnit, 2
      optional :generation_discrete_control, GenerationDiscreteControl, 3
    end
  end
  
  struct GenerationReading
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :conducting_equipment_terminal_reading, Commonmodule::ConductingEquipmentTerminalReading, 1
      optional :phase_mmtn, Commonmodule::PhaseMMTN, 2
      optional :reading_mmtr, Commonmodule::ReadingMMTR, 3
      optional :reading_mmxu, Commonmodule::ReadingMMXU, 4
    end
  end
  
  struct GenerationReadingProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :reading_message_info, Commonmodule::ReadingMessageInfo, 1
      optional :generating_unit, GeneratingUnit, 2
      optional :generation_reading, GenerationReading, 3
    end
  end
  
  struct GenerationPointStatus
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :black_start_enabled, Commonmodule::StatusSPS, 1
      optional :frequency_set_point_enabled, Commonmodule::StatusSPS, 2
      optional :pct_hz_droop, Google::Protobuf::FloatValue, 3
      optional :pct_v_droop, Google::Protobuf::FloatValue, 4
      optional :ramp_rates, Commonmodule::RampRate, 5
      optional :reactive_pwr_set_point_enabled, Commonmodule::StatusSPS, 6
      optional :real_pwr_set_point_enabled, Commonmodule::StatusSPS, 7
      optional :state, Commonmodule::OptionalStateKind, 8
      optional :sync_back_to_grid, Commonmodule::StatusSPS, 9
      optional :trans_to_islnd_on_grid_loss_enabled, Commonmodule::StatusSPS, 10
      optional :voltage_set_point_enabled, Commonmodule::StatusSPS, 11
      optional :enter_service_operation, Commonmodule::EnterServiceAPC, 12
      optional :hz_w_operation, Commonmodule::HzWAPC, 13
      optional :limit_w_operation, Commonmodule::LimitWAPC, 14
      optional :p_f_operation, Commonmodule::PFSPC, 15
      optional :tm_hz_trip_operation, Commonmodule::TmHzCSG, 16
      optional :tm_volt_trip_operation, Commonmodule::TmVoltCSG, 17
      optional :v_ar_operation, Commonmodule::VarSPC, 18
      optional :volt_var_operation, Commonmodule::VoltVarCSG, 19
      optional :volt_w_operation, Commonmodule::VoltWCSG, 20
      optional :w_var_operation, Commonmodule::WVarCSG, 21
    end
  end
  
  struct GenerationEventAndStatusZGEN
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :logical_node_for_event_and_status, Commonmodule::LogicalNodeForEventAndStatus, 1
      optional :aux_pwr_st, Commonmodule::StatusSPS, 2
      optional :dynamic_test, Commonmodule::ENSDynamicTestKind, 3
      optional :emg_stop, Commonmodule::StatusSPS, 4
      optional :gn_syn_st, Commonmodule::StatusSPS, 5
      optional :point_status, GenerationPointStatus, 6
      optional :alrm, Commonmodule::OptionalAlrmKind, 7
      optional :grid_connection_state, Commonmodule::OptionalGridConnectionStateKind, 8
      optional :man_alrm_info, Google::Protobuf::StringValue, 9
      optional :operating_state, Commonmodule::OptionalOperatingStateKind, 10
    end
  end
  
  struct GenerationEventZGEN
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :generation_event_and_status_zgen, GenerationEventAndStatusZGEN, 1
    end
  end
  
  struct GenerationEvent
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :event_value, Commonmodule::EventValue, 1
      optional :generation_event_zgen, GenerationEventZGEN, 2
    end
  end
  
  struct GenerationEventProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :event_message_info, Commonmodule::EventMessageInfo, 1
      optional :generating_unit, GeneratingUnit, 2
      optional :generation_event, GenerationEvent, 3
    end
  end
  
  struct GenerationStatusZGEN
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :generation_event_and_status_zgen, GenerationEventAndStatusZGEN, 1
    end
  end
  
  struct GenerationStatus
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :status_value, Commonmodule::StatusValue, 1
      optional :generation_status_zgen, GenerationStatusZGEN, 2
    end
  end
  
  struct GenerationStatusProfile
    include ::Protobuf::Message
    
    contract_of "proto3" do
      optional :status_message_info, Commonmodule::StatusMessageInfo, 1
      optional :generating_unit, GeneratingUnit, 2
      optional :generation_status, GenerationStatus, 3
    end
  end
  end
